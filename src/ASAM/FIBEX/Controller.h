/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "platform.h"

#include <string>
#include <xercesc/dom/DOM.hpp>

#include "ho/NameDetails.h"

#include "asam_fibex_export.h"

namespace ASAM {
namespace FIBEX {

/**
 * CONTROLLER
 */
class ASAM_FIBEX_EXPORT Controller : public ho::NameDetails
{
public:
    Controller();

    /** load from XML DOM element */
    bool load(xercesc::DOMElement * element);

    /** ID */
    std::string id;

    /** xsi:type */
    std::string xsiType;

    /** can */
    struct Can {
        Can();

        /** TIME-SEG0 */
        unsigned int timeSeg0;

        /** TIME-SEG1 */
        unsigned int timeSeg1;

        /** SYNC-JUMP-WIDTH */
        unsigned int syncJumpWidth;

        /** NUMBER-OF-SAMPLES */
        unsigned short numberOfSamples;
    } can;

    /** flexray */
    struct Flexray {
        Flexray();

        /** KEY-SLOT-USAGE */
        enum class KeySlotUsage {
            /** STARTUP-SYNC */
            StartupSync,

            /** SYNC */
            Sync,

            /** NONE */
            None
        } keySlotUsage;

        /** on STARTUP-SYNC or or SYNC */
        unsigned short keySlotId;

        /** MAX-DYNAMIC-PAYLOAD-LENGTH */
        unsigned short maxDynamicPayloadLength;

        /** CLUSTER-DRIFT-DAMPING */
        unsigned short clusterDriftDamping;

        /** DECODING-CORRECTION */
        unsigned short decodingCorrection;

        /** LISTEN-TIMEOUT */
        unsigned int listenTimeout;

        /** MAX-DRIFT */
        unsigned short maxDrift;

        /** EXTERN-OFFSET-CORRECTION */
        unsigned short externOffsetCorrection;

        /** EXTERN-RATE-CORRECTION */
        unsigned short externRateCorrection;

        /** LATEST-TX */
        unsigned short latestTx;

        /** MICRO-PER-CYCLE */
        unsigned int microPerCycle;

        /** OFFSET-CORRECTION-OUT */
        unsigned short offsetCorrectionOut;

        /** RATE-CORRECTION-OUT */
        unsigned short rateCorrectionOut;

        /** SAMPLES-PER-MICROTICK */
        unsigned short samplesPerMicrotick;

        /** DELAY-COMPENSATION-A */
        unsigned short delayCompensationA;

        /** DELAY-COMPENSATION-B */
        unsigned short delayCompensationB;

        /** WAKE-UP-PATTERN */
        unsigned short wakeUpPattern;

        /** ALLOW-HALT-DUE-TO-CLOCK */
        bool allowHaltDueToClock;

        /** ALLOW-PASSIVE-TO-ACTIVE */
        unsigned short allowPassiveToActive;

        /** ACCEPTED-STARTUP-RANGE */
        unsigned short acceptedStartupRange;

        /** MACRO-INITIAL-OFFSET-A */
        unsigned short macroInitialOffsetA;

        /** MACRO-INITIAL-OFFSET-B */
        unsigned short macroInitialOffsetB;

        /** MICRO-INITIAL-OFFSET-A */
        unsigned short microInitialOffsetA;

        /** MICRO-INITIAL-OFFSET-B */
        unsigned short microInitialOffsetB;

        /** SINGLE-SLOT-ENABLED */
        bool singleSlotEnabled;

        /** MICROTICK */
        float microtick;

        /** MICRO-PER-MACRO-NOM */
        float microPerMacroNom;
    } flexray;
};

}
}
