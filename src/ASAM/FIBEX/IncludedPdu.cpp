/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "IncludedPdu.h"

namespace ASAM {
namespace FIBEX {

IncludedPdu::Fx::Fx() :
    pduTriggeringRef(),
    includedSignals(),
    completePdu(false)
{
}

IncludedPdu::IncludedPdu() :
    fx()
{
}

bool IncludedPdu::load(xercesc::DOMElement * element)
{
    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());

        /* pduTriggeringRef */
        if (nodeName == "fx:PDU-TRIGGERING-REF") {
            std::string id2 = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("ID-REF")));
            fx.pduTriggeringRef = id2;
        } else

        /* includedSignals */
        if (nodeName == "fx:INCLUDED-SIGNALS") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                IncludedSignal includedSignal;
                includedSignal.load(childElement2);
                fx.includedSignals.insert(includedSignal);
            }
        } else

        /* completePdu */
        if (nodeName == "fx:COMPLETE-PDU") {
            fx.completePdu = true;
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }

    return true;
}

bool IncludedPdu::operator < (const IncludedPdu & rhs) const
{
    return fx.pduTriggeringRef < rhs.fx.pduTriggeringRef;
}

}
}
