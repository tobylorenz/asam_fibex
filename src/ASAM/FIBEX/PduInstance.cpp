/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "PduInstance.h"

namespace ASAM {
namespace FIBEX {

PduInstance::Fx::Fx() :
    pduRef(),
    bitPosition(),
    isHighLowByteOrder(),
    pduUpdateBitPosition()
{
}

PduInstance::PduInstance() :
    id(),
    fx()
{
}

bool PduInstance::load(xercesc::DOMElement * element)
{
    /* read attributes */
    id = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("ID")));

    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());
        std::string value = xercesc::XMLString::transcode(childElement->getTextContent());

        /* pduRef */
        if (nodeName == "fx:PDU-REF") {
            std::string id2 = xercesc::XMLString::transcode(childElement->getAttribute(xercesc::XMLString::transcode("ID-REF")));
            fx.pduRef = id2;
        } else

        /* bitPosition */
        if (nodeName == "fx:BIT-POSITION") {
            fx.bitPosition = stoul(value);
        } else

        /* isHighLowByteOrder */
        if (nodeName == "fx:IS-HIGH-LOW-BYTE-ORDER") {
            fx.isHighLowByteOrder = (value == "true");
        } else

        /* pduUpdateBitPosition */
        if (nodeName == "fx:PDU-UPDATE-BIT-POSITION") {
            fx.pduUpdateBitPosition = stoul(value);
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }

    return true;
}

}
}
