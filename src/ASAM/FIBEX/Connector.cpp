/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "Connector.h"

namespace ASAM {
namespace FIBEX {

Connector::Fx::Fx() :
    channelRef(),
    controllerRef(),
    inputs()
{
}

Connector::Flexray::Flexray() :
    wakeUpChannel()
{
}

Connector::Connector() :
    id(),
    xsiType(),
    fx(),
    flexray()
{
}

bool Connector::load(xercesc::DOMElement * element)
{
    /* read attributes */
    id = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("ID")));
    xsiType = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("xsi:type")));

    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());

        /* channelRef */
        if (nodeName == "fx:CHANNEL-REF") {
            std::string id2 = xercesc::XMLString::transcode(childElement->getAttribute(xercesc::XMLString::transcode("ID-REF")));
            fx.channelRef = id2;
        } else

        /* controllerRef */
        if (nodeName == "fx:CONTROLLER-REF") {
            std::string id2 = xercesc::XMLString::transcode(childElement->getAttribute(xercesc::XMLString::transcode("ID-REF")));
            fx.controllerRef = id2;
        } else

        if (nodeName == "fx:INPUTS") {
            /* read childs */
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string id2 = xercesc::XMLString::transcode(childElement2->getAttribute(xercesc::XMLString::transcode("ID")));
                fx.inputs[id2].load(childElement2);
            }
        } else

        if (nodeName == "fx:OUTPUTS") {
            /* read childs */
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string id2 = xercesc::XMLString::transcode(childElement2->getAttribute(xercesc::XMLString::transcode("ID")));
                fx.outputs[id2].load(childElement2);
            }
        } else

        if (nodeName == "flexray:WAKE-UP-CHANNEL") {
            std::string value = xercesc::XMLString::transcode(childElement->getTextContent());
            flexray.wakeUpChannel = (value == "true");
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }

    return true;
}

}
}
