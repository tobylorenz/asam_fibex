/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "AbsolutelyScheduledTiming.h"

namespace ASAM {
namespace FIBEX {

AbsolutelyScheduledTiming::Fx::Fx() :
    slotId(),
    baseCycle(),
    cycleRepetition()
{
}

AbsolutelyScheduledTiming::AbsolutelyScheduledTiming() :
    fx()
{
}

bool AbsolutelyScheduledTiming::load(xercesc::DOMElement * element)
{
    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());
        std::string value = xercesc::XMLString::transcode(childElement->getTextContent());

        /* slotId */
        if (nodeName == "fx:SLOT-ID") {
            fx.slotId = stoul(value);
        } else

        /* baseCycle */
        if (nodeName == "fx:BASE-CYCLE") {
            fx.baseCycle = stoul(value);
        } else

        /* cycleRepetition */
        if (nodeName == "fx:CYCLE-REPETITION") {
            fx.cycleRepetition = stoul(value);
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }

    return true;
}

}
}
