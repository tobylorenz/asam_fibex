/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "EcuSendingBehavior.h"

namespace ASAM {
namespace FIBEX {

EcuSendingBehavior::EcuSendingBehavior() :
    systemState(),
    value()
{
}

bool EcuSendingBehavior::load(xercesc::DOMElement * element)
{
    std::string nodeName = xercesc::XMLString::transcode(element->getNodeName());

    /* read attributes */
    systemState = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("SYSTEM-STATE")));
    std::string strValue = xercesc::XMLString::transcode(element->getTextContent());

    if (strValue == "ERROR") {
        value = Value::Error;
    } else

    if (strValue == "DEFAULT") {
        value = Value::Default;
    } else

    if (strValue == "LAST-VALUE") {
        value = Value::LastValue;
    } else

    if (strValue == "NOT-AVAILABLE") {
        value = Value::NotAvailable;
    } else

    if (strValue == "NOT-DEFINED") {
        value = Value::NotDefined;
    } else

    if (strValue == "NOT-VALID") {
        value = Value::NotValid;
    } else

    if (strValue == "OTHER") {
        value = Value::Other;
    } else

    if (strValue == "VALID") {
        value = Value::Valid;
    } else

    {
        std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
    }

    return true;
}

bool EcuSendingBehavior::operator < (const EcuSendingBehavior & rhs) const
{
    return systemState < rhs.systemState;
}
}
}
