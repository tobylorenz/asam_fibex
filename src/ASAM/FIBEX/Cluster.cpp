/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "Cluster.h"

namespace ASAM {
namespace FIBEX {

Cluster::Fx::Fx() :
    speed(),
    isHighLowBitOrder(),
    bitCountingPolicy(),
    protocol(),
    protocolVersion(),
    physical(),
    physicalVersion(),
    channelRefs(),
    medium(),
    numberOfCycles(),
    maxFrameLength()
{
}

Cluster::Flexray::WakeUp::WakeUp() :
    wakeUpSymbolRxIdle(14),
    wakeUpSymbolRxLow(10),
    wakeUpSymbolRxWindow(82),
    wakeUpSymbolTxIdle(),
    wakeUpSymbolTxLow()
{
}

Cluster::Flexray::Flexray() :
    coldStartAttempts(16),
    actionPointOffset(32),
    dynamicSlotIdlePhase(0),
    minislot(16),
    minislotActionPointOffset(16),
    nit(256),
    sampleClockPeriod(),
    staticSlot(512),
    symbolWindow(1),
    tssTransmitter(5),
    wakeUp(),
    listenNoise(8),
    macroPerCycle(8000),
    macrotick(1.0),
    maxInitializationError(0.0),
    maxWithoutClockCorrectionFatal(1),
    maxWithoutClockCorrectionPassive(1),
    networkManagementVectorLength(0),
    numberOfMinislots(4096),
    numberOfStaticSlots(512),
    offsetCorrectionStart(8192),
    payloadLengthStatic(64),
    syncNodeMax(8),
    casRxLowMax(),
    bit(),
    cycle(8192),
    clusterDriftDamping(0)
{
}

Cluster::Cluster() :
    ho::NameDetails(),
    id(),
    xsiType(),
    fx(),
    flexray()
{
}

bool Cluster::load(xercesc::DOMElement * element)
{
    ho::NameDetails::load(element);

    /* read attributes */
    id = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("ID")));
    xsiType = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("xsi:type")));

    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());
        std::string value = xercesc::XMLString::transcode(childElement->getTextContent());

        /* speeed */
        if (nodeName == "fx:SPEED") {
            fx.speed = stoul(value);
        } else

        /* isHighLowBitOrder */
        if (nodeName == "fx:IS-HIGH-LOW-BIT-ORDER") {
            fx.isHighLowBitOrder = (value == "true");
        } else

        /* bitCountingPolicy */
        if (nodeName == "fx:BIT-COUNTING-POLICY") {
            if (value == "MONOTONE") {
                fx.bitCountingPolicy = Fx::BitCountingPolicy::Monotone;
            } else
            if (value == "SAWTOOTH") {
                fx.bitCountingPolicy = Fx::BitCountingPolicy::Sawtooth;
            } else
            {
                std::cerr << "Unrecognized nodeName " << nodeName << " in " << xercesc::XMLString::transcode(childElement->getBaseURI()) << std::endl;
            }
        } else

        /* protocol */
        if (nodeName == "fx:PROTOCOL") {
            fx.protocol = value;
        } else

        /* protocolVersion */
        if (nodeName == "fx:PROTOCOL-VERSION") {
            fx.protocolVersion = value;
        } else

        /* channelRefs */
        if (nodeName == "fx:CHANNEL-REFS") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string nodeName2 = xercesc::XMLString::transcode(childElement2->getNodeName());
                std::string idRef = xercesc::XMLString::transcode(childElement2->getAttribute(xercesc::XMLString::transcode("ID-REF")));

                /* channelRef */
                if (nodeName2 == "fx:CHANNEL-REF") {
                    fx.channelRefs.push_back(idRef);
                } else

                {
                    std::cerr << "Unrecognized nodeName " << nodeName2 << " in " << xercesc::XMLString::transcode(childElement2->getBaseURI()) << std::endl;
                }
            }
        } else

        /* medium */
        if (nodeName == "fx:MEDIUM") {
            if (value == "ELECTRICAL") {
                fx.medium = Fx::Medium::Electrical;
            } else
            if (value == "OPTICAL") {
                fx.medium = Fx::Medium::Optical;
            } else
            if (value == "OTHER") {
                fx.medium = Fx::Medium::Other;
            } else
            {
                std::cerr << "Unrecognized nodeName " << nodeName << " in " << xercesc::XMLString::transcode(childElement->getBaseURI()) << std::endl;
            }
        } else

        /* numberOfCycles */
        if (nodeName == "fx:NUMBER-OF-CYCLES") {
            fx.numberOfCycles = stoul(value);
        } else

        /* maxframeLength */
        if (nodeName == "fx:MAX-FRAME-LENGTH") {
            fx.maxFrameLength = stoul(value);
        } else

        /* coldStartAttempts */
        if (nodeName == "flexray:COLD-START-ATTEMPTS") {
            flexray.coldStartAttempts = stoul(value);
        } else

        /* actionPointOffset */
        if (nodeName == "flexray:ACTION-POINT-OFFSET") {
            flexray.actionPointOffset = stoul(value);
        } else

        /* dynamicSlotIdlePhase */
        if (nodeName == "flexray:DYNAMIC-SLOT-IDLE-PHASE") {
            flexray.dynamicSlotIdlePhase = stoul(value);
        } else

        /* minislot */
        if (nodeName == "flexray:MINISLOT") {
            flexray.minislot = stoul(value);
        } else

        /* minislotActionPointOffset */
        if (nodeName == "flexray:MINISLOT-ACTION-POINT-OFFSET") {
            flexray.minislotActionPointOffset = stoul(value);
        } else

        /* nit */
        if (nodeName == "flexray:N-I-T") {
            flexray.nit = stoul(value);
        } else

        /* sampleClockPeriod */
        if (nodeName == "flexray:SAMPLE-CLOCK-PERIOD") {
            flexray.sampleClockPeriod = stof(value);
        } else

        /* staticSlot */
        if (nodeName == "flexray:STATIC-SLOT") {
            flexray.staticSlot = stoul(value);
        } else

        /* symbolWindow */
        if (nodeName == "flexray:SYMBOL-WINDOW") {
            flexray.symbolWindow = stoul(value);
        } else

        /* tssTransmitter */
        if (nodeName == "flexray:T-S-S-TRANSMITTER") {
            flexray.tssTransmitter = stoul(value);
        } else

        /* wakeUp */
        if (nodeName == "flexray:WAKE-UP") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string nodeName2 = xercesc::XMLString::transcode(childElement2->getNodeName());
                std::string value2 = xercesc::XMLString::transcode(childElement2->getTextContent());

                /* wakeUpSymbolRxIdle */
                if (nodeName2 == "flexray:WAKE-UP-SYMBOL-RX-IDLE") {
                    flexray.wakeUp.wakeUpSymbolRxIdle = stoul(value2);
                } else

                /* wakeUpSymbolRxLow */
                if (nodeName2 == "flexray:WAKE-UP-SYMBOL-RX-LOW") {
                    flexray.wakeUp.wakeUpSymbolRxLow = stoul(value2);
                } else

                /* wakeUpSymbolRxWindow */
                if (nodeName2 == "flexray:WAKE-UP-SYMBOL-RX-WINDOW") {
                    flexray.wakeUp.wakeUpSymbolRxWindow = stoul(value2);
                } else

                /* wakeUpSymbolTxIdle */
                if (nodeName2 == "flexray:WAKE-UP-SYMBOL-TX-IDLE") {
                    flexray.wakeUp.wakeUpSymbolTxIdle = stoul(value2);
                } else

                /* wakeUpSymbolTxLow */
                if (nodeName2 == "flexray:WAKE-UP-SYMBOL-TX-LOW") {
                    flexray.wakeUp.wakeUpSymbolTxLow = stoul(value2);
                } else

                {
                    std::cerr << "Unrecognized nodeName " << nodeName2 << " in " << xercesc::XMLString::transcode(childElement2->getBaseURI()) << std::endl;
                }
            }
        } else

        /* listenNoise */
        if (nodeName == "flexray:LISTEN-NOISE") {
            flexray.listenNoise = stoul(value);
        } else

        /* macroPerCycle */
        if (nodeName == "flexray:MACRO-PER-CYCLE") {
            flexray.macroPerCycle = stoul(value);
        } else

        /* macrotick */
        if (nodeName == "flexray:MACROTICK") {
            flexray.macrotick = stof(value);
        } else

        /* maxInitializationError */
        if (nodeName == "flexray:MAX-INITIALIZATION-ERROR") {
            flexray.maxInitializationError = stof(value);
        } else

        /* maxWithoutClockCorrectionFatal */
        if (nodeName == "flexray:MAX-WITHOUT-CLOCK-CORRECTION-FATAL") {
            flexray.maxWithoutClockCorrectionFatal = stoul(value);
        } else

        /* maxWithoutClockCorrectionPassive */
        if (nodeName == "flexray:MAX-WITHOUT-CLOCK-CORRECTION-PASSIVE") {
            flexray.maxWithoutClockCorrectionPassive = stoul(value);
        } else

        /* networkManagementVectorLength */
        if (nodeName == "flexray:NETWORK-MANAGEMENT-VECTOR-LENGTH") {
            flexray.networkManagementVectorLength = stoul(value);
        } else

        /* numberOfMinislots */
        if (nodeName == "flexray:NUMBER-OF-MINISLOTS") {
            flexray.numberOfMinislots = stoul(value);
        } else

        /* numberOfStaticSlots */
        if (nodeName == "flexray:NUMBER-OF-STATIC-SLOTS") {
            flexray.numberOfStaticSlots = stoul(value);
        } else

        /* offsetCorrectionStart */
        if (nodeName == "flexray:OFFSET-CORRECTION-START") {
            flexray.offsetCorrectionStart = stoul(value);
        } else

        /* payloadLengthStatic */
        if (nodeName == "flexray:PAYLOAD-LENGTH-STATIC") {
            flexray.payloadLengthStatic = stoul(value);
        } else

        /* syncNodeMax */
        if (nodeName == "flexray:SYNC-NODE-MAX") {
            flexray.syncNodeMax = stoul(value);
        } else

        /* casRxLowMax */
        if (nodeName == "flexray:CAS-RX-LOW-MAX") {
            flexray.casRxLowMax = stoul(value);
        } else

        /* bit */
        if (nodeName == "flexray:BIT") {
            flexray.bit = stof(value);
        } else

        /* cycle */
        if (nodeName == "flexray:CYCLE") {
            flexray.cycle = stoul(value);
        } else

        /* clusterDriftDamping */
        if (nodeName == "flexray:CLUSTER-DRIFT-DAMPING") {
            flexray.clusterDriftDamping = stoul(value);
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }

    return true;
}

}
}
