/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "platform.h"

#include <map>
#include <string>
#include <xercesc/dom/DOM.hpp>

#include "InputPort.h"
#include "OutputPort.h"

#include "asam_fibex_export.h"

namespace ASAM {
namespace FIBEX {

/**
 * CONNECTOR
 */
class ASAM_FIBEX_EXPORT Connector
{
public:
    Connector();

    /** load from XML DOM element */
    bool load(xercesc::DOMElement * element);

    /** ID */
    std::string id;

    /** xsi:type */
    std::string xsiType;

    /** fx */
    struct Fx {
        Fx();

        /** CHANNEL-REF */
        std::string channelRef;

        /** CONTROLLER-REF */
        std::string controllerRef;

        /** INPUTS */
        std::map<std::string, InputPort> inputs;

        /** OUTPUTS */
        std::map<std::string, OutputPort> outputs;
    } fx;

    /** flexray */
    struct Flexray {
        Flexray();

        /** WAKE-UP-CHANNEL */
        bool wakeUpChannel;
    } flexray;
};

}
}
