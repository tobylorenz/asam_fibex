#pragma once

#include <string>

namespace ASAM {
namespace FIBEX {

/**
 * @brief GW-INTERNAL
 *
 * A flag indicating that the source resp. target of a GW-MAPPING is internaly
 * inside of the gateway application.
 */
typedef bool GwInternal;

/**
 * @brief TRIGGER-CONDITION
 *
 * Gateway specific trigger condition of target destination
 */
enum class TriggerCondition {
    Immediate,
    OnChange,
    None
};

namespace ho {

/**
 * @brief simpleType ho:FULL-STRING
 *
 * For uniformity we use 3 types of String. This is the usual one supporting
 * strings up to 255 chars.
 */
typedef std::string FullString;

/**
 * @brief simpleType ho:IDENTIFIER-TYPE
 */
typedef std::string IdentifierType;

/**
 * @brief simpleType ho:LONG-STRING
 *
 * For uniformity we use 3 types of String. This is the long one supporting
 * strings up to 65335 chars.
 */
typedef std::string LongString;

/**
 * @brief simpleType ho:SHORT-STRING
 *
 * For uniformity we use 3 types of String. This is the short one supporting
 * strings up to 128 chars.
 */
typedef std::string ShortString;

/**
 * @brief simpleType ho:VERSION-LABEL-TYPE
 *
 * This type ensures the ASAM policy for version labels. An ASAM Version label
 * consists in an application profile number, a version number, a revision
 * number and optionaly a patch level.
 */
typedef std::string VersionLabelType;

}

namespace fx {

/**
 * @brief element fx:BIT-POSITION
 *
 * A signal's/PDU's bit position relatively to the beginning of the containing
 * element (i.e., PDU/FRAME/SIGNAL-GROUP)
 */
typedef unsigned short BitPosition;

/**
 * @brief element fx:BYTE-VALUE
 *
 * The integer value of a freely defined data byte.
 */
typedef unsigned short ByteValue;

/**
 * @brief element fx:FRAME-REF
 *
 * Refering a frame
 */
typedef std::string FrameRef;

/**
 * @brief complexType fx:IDENTIFIER-VALUE-TYPE
 *
 * A frames unique id on a channel. The usage depends on the communication
 * system. E.g. on a CAN or LIN cluster this parameter represents the CAN (resp.
 * LIN) ID in the frames header section. On a FlexRay channel this parameter
 * represents the frames (optional) messageId in the leading word of the frames
 * data part.
 */
typedef long IdentifierValueType;

struct ManufacturerIdentifierExtension;

/**
 * @brief element fx:IDENTIFIER
 *
 * To describe a frames identifier on the communication system, usualy with a
 * fixed identifierValue.
 */
struct Identifier {
    IdentifierValueType identifierValue;

    /**
     * Manufacturers may mark up here alternative identification strategies
     * instead of exporting a fixed identifier value
     */
    ManufacturerIdentifierExtension manufacturerExtension;
};
}

}
}
