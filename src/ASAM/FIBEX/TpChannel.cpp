/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "TpChannel.h"

namespace ASAM {
namespace FIBEX {

TpChannel::Asr30tp::Asr30tp() :
    channelId(),
    timeoutBs(),
    timeoutCr(),
    transmitCancellation(),
    tpConnections(),
    tpPduUsages()
{
}

TpChannel::Asr30frtp::Asr30frtp() :
    acktype(),
    addressingType(),
    groupSegmentation(),
    maxBlockSize(),
    maxRetries(),
    maximumMessageLength()
{
}

TpChannel::TpChannel() :
    ho::NameDetails(),
    asr30tp(),
    asr30frtp()
{
}

bool TpChannel::load(xercesc::DOMElement * element)
{
    ho::NameDetails::load(element);

    /* read attributes */
    id = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("ID")));
    xsiType = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("xsi:type")));

    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());
        std::string value = xercesc::XMLString::transcode(childElement->getTextContent());

        /* channelId */
        if (nodeName == "asr30tp:CHANNEL-ID") {
            asr30tp.channelId = stoul(value);
        } else

        /* timeoutBs */
        if (nodeName == "asr30tp:TIMEOUT-BS") {
            asr30tp.timeoutBs = value;
        } else

        /* timeoutCr */
        if (nodeName == "asr30tp:TIMEOUT-CR") {
            asr30tp.timeoutCr = value;
        } else

        /* transmitCancellation */
        if (nodeName == "asr30tp:TRANSMIT-CANCELLATION") {
            asr30tp.transmitCancellation = (value == "true");
        } else

        /* tpConnections */
        if (nodeName == "asr30tp:TP-CONNECTIONS") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string id2 = xercesc::XMLString::transcode(childElement2->getAttribute(xercesc::XMLString::transcode("ID")));
                asr30tp.tpConnections[id2].load(childElement2);
            }
        } else

        /* tpPduUsages */
        if (nodeName == "asr30tp:TP-PDU-USAGES") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string id2 = xercesc::XMLString::transcode(childElement2->getAttribute(xercesc::XMLString::transcode("ID")));
                asr30tp.tpPduUsages[id2].load(childElement2);
            }
        } else

        /* acktype */
        if (nodeName == "asr30frtp:ACKTYPE") {
            if (value == "FRTP_NO") {
                asr30frtp.acktype = Asr30frtp::Acktype::FrtpNo;
            } else

            if (value == "FRTP_ACK_WITHOUT_RT") {
                asr30frtp.acktype = Asr30frtp::Acktype::FrtpAckWithoutRt;
            } else

            if (value == "FRTP_ACK_WITH_RT") {
                asr30frtp.acktype = Asr30frtp::Acktype::FrtpAckWithRt;
            } else

            {
                std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
            }
        } else

        /* addressingType */
        if (nodeName == "asr30frtp:ADDRESSING-TYPE") {
            if (value == "FRTP_OB") {
                asr30frtp.addressingType = Asr30frtp::AdressingType::FrtpOb;
            } else

            if (value == "FRTP_TB") {
                asr30frtp.addressingType = Asr30frtp::AdressingType::FrtpTb;
            } else

            {
                std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
            }
        } else

        /* groupSegmentation */
        if (nodeName == "asr30frtp:GROUP-SEGMENTATION") {
            asr30frtp.groupSegmentation = (value == "true");
        } else

        /* maxBlockSize */
        if (nodeName == "asr30frtp:MAX-BLOCK-SIZE") {
            asr30frtp.maxBlockSize = stoul(value);
        } else

        /* maxRetries */
        if (nodeName == "asr30frtp:MAX-RETRIES") {
            asr30frtp.maxRetries = stoul(value);
        } else

        /* maximumMessageLength */
        if (nodeName == "asr30frtp:MAXIMUM-MESSAGE-LENGTH") {
            if (value == "FRTP_ISO") {
                asr30frtp.maximumMessageLength = Asr30frtp::MaximumMessageLength::FrtpIso;
            } else

            if (value == "FRTP_ISO6") {
                asr30frtp.maximumMessageLength = Asr30frtp::MaximumMessageLength::FrtpIso6;
            } else

            if (value == "FRTP_L4G") {
                asr30frtp.maximumMessageLength = Asr30frtp::MaximumMessageLength::FrtpL4g;
            } else

            {
                std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
            }
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }

    return true;
}

}
}
