/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "CompuMethod.h"

namespace ASAM {
namespace FIBEX {

CompuMethod::Ho::Ho() :
    category(),
    internalConstrs(),
    compuInternalToPhys()
{
}

CompuMethod::CompuMethod() :
    ho::NameDetails(),
    ho()
{
}

bool CompuMethod::load(xercesc::DOMElement * element)
{
    ho::NameDetails::load(element);

    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());
        std::string value = xercesc::XMLString::transcode(childElement->getTextContent());

        /* category */
        if (nodeName == "ho:CATEGORY") {
            if (value == "IDENTICAL") {
                ho.category = Ho::Category::Identical;
            } else
            if (value == "LINEAR") {
                ho.category = Ho::Category::Linear;
            } else
            if (value == "SCALE-LINEAR") {
                ho.category = Ho::Category::ScaleLinear;
            } else
            if (value == "TEXTTABLE") {
                ho.category = Ho::Category::Texttable;
            } else
            if (value == "TAB-NOINTP") {
                ho.category = Ho::Category::TabNointp;
            } else
            if (value == "FORMULA") {
                ho.category = Ho::Category::Formula;
            } else
            {
                std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
            }
        } else

        /* unitRef */
        if (nodeName == "ho:UNIT-REF") {
            std::string id2 = xercesc::XMLString::transcode(childElement->getAttribute(xercesc::XMLString::transcode("ID-REF")));
            ho.unitRef = id2;
        } else

        /* internalConstrs */
        if (nodeName == "ho:INTERNAL-CONSTRS") {
            ho.internalConstrs.load(childElement);
        } else

        /* compuInternalToPhys */
        if (nodeName == "ho:COMPU-INTERNAL-TO-PHYS") {
            ho.compuInternalToPhys.load(childElement);
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }

    return true;

}

}
}
