/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "Timings.h"

namespace ASAM {
namespace FIBEX {

Timings::Fx::Fx() :
    cyclicTiming(),
    eventControlledTiming(),
    absolutelyScheduledTiming()
{
}

Timings::Timings() :
    fx()
{
}

bool Timings::load(xercesc::DOMElement * element)
{
    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());

        /* cyclicTiming */
        if (nodeName == "fx:CYCLIC-TIMING") {
            fx.cyclicTiming.load(childElement);
        } else

        /* eventControlledTiming */
        if (nodeName == "fx:EVENT-CONTROLLED-TIMING") {
            fx.eventControlledTiming.load(childElement);
        } else

        /* absolutelyScheduledTiming */
        if (nodeName == "fx:ABSOLUTELY-SCHEDULED-TIMING") {
            fx.absolutelyScheduledTiming.load(childElement);
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }

    return true;
}

}
}
