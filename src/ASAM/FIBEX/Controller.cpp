/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "Controller.h"

namespace ASAM {
namespace FIBEX {

Controller::Can::Can() :
    timeSeg0(1),
    timeSeg1(2),
    syncJumpWidth(58),
    numberOfSamples(1)
{
}

Controller::Flexray::Flexray() :
    keySlotUsage(),
    keySlotId(),
    maxDynamicPayloadLength(),
    clusterDriftDamping(),
    decodingCorrection(28),
    listenTimeout(),
    maxDrift(32),
    externOffsetCorrection(0),
    externRateCorrection(0),
    latestTx(),
    microPerCycle(6400),
    offsetCorrectionOut(8196),
    rateCorrectionOut(2),
    samplesPerMicrotick(),
    delayCompensationA(),
    delayCompensationB(),
    wakeUpPattern(32),
    allowHaltDueToClock(),
    allowPassiveToActive(0),
    acceptedStartupRange(),
    macroInitialOffsetA(2),
    macroInitialOffsetB(2),
    microInitialOffsetA(),
    microInitialOffsetB(),
    singleSlotEnabled(false),
    microtick(),
    microPerMacroNom()
{
}

Controller::Controller() :
    ho::NameDetails(),
    can(),
    flexray()
{
}

bool Controller::load(xercesc::DOMElement * element)
{
    ho::NameDetails::load(element);

    /* read attributes */
    id = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("ID")));
    xsiType = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("xsi:type")));

    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());
        std::string value = xercesc::XMLString::transcode(childElement->getTextContent());

        /* timeSeg0 */
        if (nodeName == "can:TIME-SEG0") {
            can.timeSeg0 = stoul(value);
        } else

        /* timeSeg1 */
        if (nodeName == "can:TIME-SEG1") {
            can.timeSeg1 = stoul(value);
        } else

        /* syncJumpWidth */
        if (nodeName == "can:SYNC-JUMP-WIDTH") {
            can.syncJumpWidth = stoul(value);
        } else

        /* numberOfSamples */
        if (nodeName == "can:NUMBER-OF-SAMPLES") {
            can.numberOfSamples = stoul(value);
        } else

        /* keySlotUsage */
        if (nodeName == "flexray:KEY-SLOT-USAGE") {
            xercesc::DOMElement * childElement2 = childElement->getFirstElementChild();
            std::string nodeName2 = xercesc::XMLString::transcode(childElement2->getNodeName());

            /* startupSync */
            if (nodeName2 == "flexray:STARTUP-SYNC") {
                flexray.keySlotUsage = Flexray::KeySlotUsage::StartupSync;
                std::string value2 = xercesc::XMLString::transcode(childElement2->getTextContent());
                flexray.keySlotId = stoul(value2);
            } else

            /* sync */
            if (nodeName2 == "flexray:SYNC") {
                flexray.keySlotUsage = Flexray::KeySlotUsage::Sync;
                std::string value2 = xercesc::XMLString::transcode(childElement2->getTextContent());
                flexray.keySlotId = stoul(value2);
            } else

            /* none */
            if (nodeName2 == "flexray:NONE") {
                flexray.keySlotUsage = Flexray::KeySlotUsage::None;
            } else

            {
                std::cerr << "Unrecognized nodeName " << nodeName2 << " in " << xercesc::XMLString::transcode(childElement2->getBaseURI()) << std::endl;
            }
        } else

        /* maxDynamicPayloadLength */
        if (nodeName == "flexray:MAX-DYNAMIC-PAYLOAD-LENGTH") {
            flexray.maxDynamicPayloadLength = stoul(value);
        } else

        /* clusterDriftDamping */
        if (nodeName == "flexray:CLUSTER-DRIFT-DAMPING") {
            flexray.clusterDriftDamping = stoul(value);
        } else

        /* decodingCorrection */
        if (nodeName == "flexray:DECODING-CORRECTION") {
            flexray.decodingCorrection = stoul(value);
        } else

        /* listenTimeout */
        if (nodeName == "flexray:LISTEN-TIMEOUT") {
            flexray.listenTimeout = stoul(value);
        } else

        /* maxDrift */
        if (nodeName == "flexray:MAX-DRIFT") {
            flexray.maxDrift = stoul(value);
        } else

        /* externOffsetCorrection */
        if (nodeName == "flexray:EXTERN-OFFSET-CORRECTION") {
            flexray.externOffsetCorrection = stoul(value);
        } else

        /* externRateCorrection */
        if (nodeName == "flexray:EXTERN-RATE-CORRECTION") {
            flexray.externRateCorrection = stoul(value);
        } else

        /* latestTx */
        if (nodeName == "flexray:LATEST-TX") {
            flexray.latestTx = stoul(value);
        } else

        /* microPerCycle */
        if (nodeName == "flexray:MICRO-PER-CYCLE") {
            flexray.microPerCycle = stoul(value);
        } else

        /* offsetCorrectionOut */
        if (nodeName == "flexray:OFFSET-CORRECTION-OUT") {
            flexray.offsetCorrectionOut = stoul(value);
        } else

        /* rateCorrectionOut */
        if (nodeName == "flexray:RATE-CORRECTION-OUT") {
            flexray.rateCorrectionOut = stoul(value);
        } else

        /* samplesPerMicrotick */
        if (nodeName == "flexray:SAMPLES-PER-MICROTICK") {
            flexray.samplesPerMicrotick = stoul(value);
        } else

        /* delayCompensationA */
        if (nodeName == "flexray:DELAY-COMPENSATION-A") {
            flexray.delayCompensationA = stoul(value);
        } else

        /* delayCompensationB */
        if (nodeName == "flexray:DELAY-COMPENSATION-B") {
            flexray.delayCompensationB = stoul(value);
        } else

        /* wakeUpPattern */
        if (nodeName == "flexray:WAKE-UP-PATTERN") {
            flexray.wakeUpPattern = stoul(value);
        } else

        /* allowHaltDueToClock */
        if (nodeName == "flexray:ALLOW-HALT-DUE-TO-CLOCK") {
            flexray.allowHaltDueToClock = (value == "true");
        } else

        /* allowPassiveToActive */
        if (nodeName == "flexray:ALLOW-PASSIVE-TO-ACTIVE") {
            flexray.allowPassiveToActive = stoul(value);
        } else

        /* acceptedStartupRange */
        if (nodeName == "flexray:ACCEPTED-STARTUP-RANGE") {
            flexray.acceptedStartupRange = stoul(value);
        } else

        /* macroInitialOffsetA */
        if (nodeName == "flexray:MACRO-INITIAL-OFFSET-A") {
            flexray.macroInitialOffsetA = stoul(value);
        } else

        /* macroInitialOffsetB */
        if (nodeName == "flexray:MACRO-INITIAL-OFFSET-B") {
            flexray.macroInitialOffsetB = stoul(value);
        } else

        /* microInitialOffsetA */
        if (nodeName == "flexray:MICRO-INITIAL-OFFSET-A") {
            flexray.microInitialOffsetA = stoul(value);
        } else

        /* microInitialOffsetB */
        if (nodeName == "flexray:MICRO-INITIAL-OFFSET-B") {
            flexray.microInitialOffsetB = stoul(value);
        } else

        /* singleSlotEnabled */
        if (nodeName == "flexray:SINGLE-SLOT-ENABLED") {
            flexray.singleSlotEnabled = (value == "true");
        } else

        /* microtick */
        if (nodeName == "flexray:MICROTICK") {
            flexray.microtick = stof(value);
        } else

        /* microPerMacroNom */
        if (nodeName == "flexray:MICRO-PER-MACRO-NOM") {
            flexray.microPerMacroNom = stof(value);
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }
}

}
}
