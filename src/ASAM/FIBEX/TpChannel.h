/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "platform.h"

#include <map>
#include <xercesc/dom/DOM.hpp>

#include "ho/NameDetails.h"
#include "TpConnection.h"
#include "TpPduUsage.h"

#include "asam_fibex_export.h"

namespace ASAM {
namespace FIBEX {

/**
 * TP-CHANNEL
 */
class ASAM_FIBEX_EXPORT TpChannel : public ho::NameDetails
{
public:
    TpChannel();

    /** load from XML DOM element */
    bool load(xercesc::DOMElement * element);

    /** ID */
    std::string id;

    /** xsi:type */
    std::string xsiType;

    /** asr30tp */
    struct Asr30tp {
        Asr30tp();

        /** CHANNEL-ID */
        unsigned int channelId;

        /** TIMEOUT-BS */
        std::string timeoutBs;

        /** TIMEOUT-CR */
        std::string timeoutCr;

        /** TRANSMIT-CANCELLATION */
        bool transmitCancellation;

        /** TP-CONNECTIONS */
        std::map<std::string, TpConnection> tpConnections;

        /** TP-PDU-USAGES */
        std::map<std::string, TpPduUsage> tpPduUsages;
    } asr30tp;

    /** asr30frtp */
    struct Asr30frtp {
        Asr30frtp();

        /** ACKTYPE */
        enum class Acktype {
            /** FRTP_NO */
            FrtpNo,

            /** FRTP_ACK_WITHOUT_RT */
            FrtpAckWithoutRt,

            /** FRTP_ACK_WITH_RT */
            FrtpAckWithRt
        } acktype;

        /** ADDRESSING-TYPE */
        enum class AdressingType {
            /** FRTP_OB */
            FrtpOb,

            /** FRTP_TB */
            FrtpTb
        } addressingType;

        /** GROUP-SEGMENTATION */
        bool groupSegmentation;

        /** MAX-BLOCK-SIZE */
        unsigned short maxBlockSize;

        /** MAX-RETRIES */
        unsigned short maxRetries;

        /** MAXIMUM-MESSAGE-LENGTH */
        enum class MaximumMessageLength {
            /** FRTP_ISO */
            FrtpIso,

            /** FRTP_ISO6 */
            FrtpIso6,

            /** FRTP_L4G */
            FrtpL4g
        } maximumMessageLength;
    } asr30frtp;
};

}
}
