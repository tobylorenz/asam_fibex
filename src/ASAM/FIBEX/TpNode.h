/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "platform.h"

#include <set>
#include <string>
#include <xercesc/dom/DOM.hpp>

#include "ho/NameDetails.h"

#include "asam_fibex_export.h"

namespace ASAM {
namespace FIBEX {

/**
 * TP-NODE
 */
class ASAM_FIBEX_EXPORT TpNode : public ho::NameDetails
{
public:
    TpNode();

    /** load from XML DOM element */
    bool load(xercesc::DOMElement * element);

    /** ID */
    std::string id;

    /** xsi:type */
    std::string xsiType;

    /** asr30tp */
    struct Asr30tp {
        Asr30tp();

        /** STMIN */
        unsigned short stmin;

        /** TIMEOUT-AR */
        std::string timeoutAr;

        /** TIMEOUT-AS */
        std::string timeoutAs;

        /** CONNECTOR-REFS */
        std::set<std::string> connectorRefs;
    } asr30tp;

    /** fx */
    struct Fx {
        Fx();

        /** TP-ADDRESS-REF */
        struct TpAddressRef {
            TpAddressRef();

            /** ADDRESSING */
            enum class Addressing {
                /** PHYSICAL */
                Physical,

                /** FUNCTIONAL */
                Functional,

                /** TESTER */
                Tester,

                /** OTHER */
                Other
            } addressing;

            /** ID-REF */
            std::string idRef;
        } tpAddressRef;
    } fx;

    /** asr30frtp */
    struct Asr30Frtp {
        Asr30Frtp();

        /** BUFFER-REQUEST */
        unsigned short bufferRequest;

        /** MAX-AR */
        unsigned short maxAr;

        /** MAX-AS */
        unsigned short maxAs;

        /** MAX-FRIF */
        unsigned short maxFrif;

        /** TIME-BUFFER */
        std::string timeBuffer;

        /** TIME-FRIF */
        std::string timeFrif;
    } asr30frtp;
};

}
}
