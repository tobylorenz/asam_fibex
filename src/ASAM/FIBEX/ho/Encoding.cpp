/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Encoding.h"

namespace ASAM {
namespace FIBEX {
namespace ho {

Encoding::Encoding() :
    encoding()
{
}

void Encoding::load(xercesc::DOMElement * element)
{
    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());
        std::string value = xercesc::XMLString::transcode(childElement->getTextContent());

        /* encoding */
        if (nodeName == "ho:ENCODING") {
            if (value == "SIGNED") {
                encoding = Signed;
            } else
            if (value == "UNSIGNED") {
                encoding = Unsigned;
            } else
            if (value == "BIT") {
                encoding = Bit;
            } else
            if (value == "IEEE-FLOATING-TYPE") {
                encoding = IeeeFloatingType;
            } else
            if (value == "BCD") {
                encoding = Bcd;
            } else
            if (value == "DSP-FRACTIONAL") {
                encoding = DspFractional;
            } else
            if (value == "SM") {
                encoding = Sm;
            } else
            if (value == "BCD-P") {
                encoding = BcdP;
            } else
            if (value == "BCD-UP") {
                encoding = BcdUp;
            } else
            if (value == "1C") {
                encoding = _1C;
            } else
            if (value == "2C") {
                encoding = _2C;
            } else
            if (value == "UTF-8") {
                encoding = Utf8;
            } else
            if (value == "UCS-2") {
                encoding = Ucs2;
            } else
            if (value == "ISO-8859-1") {
                encoding = Iso8859_1;
            } else
            if (value == "ISO-8859-2") {
                encoding = Iso8859_2;
            } else
            if (value == "WINDOWS-1252") {
                encoding = Windows1252;
            }
        }
    }
}

}
}
}
