/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Validity.h"

namespace ASAM {
namespace FIBEX {
namespace ho {

Validity::Validity() :
    validity()
{
}

void Validity::load(xercesc::DOMElement * element)
{
    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());
        std::string value = xercesc::XMLString::transcode(childElement->getTextContent());

        /* validity */
        if (nodeName == "ho:VALIDITY") {
            if (value == "VALID") {
                validity = Valid;
            } else
            if (value == "NOT-VALID") {
                validity = NotValid;
            } else
            if (value == "NOT-AVAILABLE") {
                validity = NotAvailable;
            } else
            if (value == "NOT-DEFINED") {
                validity = NotDefined;
            } else
            if (value == "ERROR") {
                validity = Error;
            } else
            if (value == "OTHER") {
                validity = Other;
            }
        }
    }
}

}
}
}
