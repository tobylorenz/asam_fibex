/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "ControllerType.h"

namespace ASAM {
namespace FIBEX {
namespace can {

ControllerType::ControllerType() :
    fx::ControllerType(),
    timeSeg0(1),
    timeSeg1(2),
    syncJumpWidth(58),
    numberOfSamples(1)
{
}

void ControllerType::load(xercesc::DOMElement * element)
{
    fx::ControllerType::load(element);

    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());

        /* timeSeg0 */
        if (nodeName == "can:TIME-SEG0") {
            timeSeg0.load(childElement);
        } else

        /* timeSeg1 */
        if (nodeName == "can:TIME-SEG1") {
            timeSeg1.load(childElement);
        } else

        /* syncJumpWidth */
        if (nodeName == "can:SYNC-JUMP-WIDTH") {
            syncJumpWidth.load(childElement);
        } else

        /* numberOfSamples */
        if (nodeName == "can:NUMBER-OF-SAMPLES") {
            xs::Integer integer;
            integer.load(childElement);
            numberOfSamples.push_back(integer);
        }
    }
}

}
}
}
