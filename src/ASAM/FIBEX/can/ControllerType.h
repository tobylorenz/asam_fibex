/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "../platform.h"

#include <list>
#include <xercesc/dom/DOM.hpp>

#include "../fx/ControllerType.h"
#include "../xs/Integer.h"

#include "asam_fibex_export.h"

namespace ASAM {
namespace FIBEX {
namespace can {

/**
 * @brief complexType CONTROLLER-TYPE
 *
 * The CAN-specific extension of the common CONTROLLER-TYPE
 */
class ASAM_FIBEX_EXPORT ControllerType : public fx::ControllerType
{
public:
    ControllerType();

    /** load from XML DOM element */
    void load(xercesc::DOMElement * element);

    /**
     * @brief TIME-SEG0
     *
     * Time segment 0 in time quantums (TQ).
     */
    xs::Integer timeSeg0;

    /**
     * @brief TIME-SEG1
     *
     * Time segment 1 in time quantums (TQ).
     */
    xs::Integer timeSeg1;

    /**
     * @brief SYNC-JUMP-WIDTH
     *
     * Synchronization jump witdh in time quantums (TQ).
     */
    xs::Integer syncJumpWidth;

    /**
     * @brief NUMBER-OF-SAMPLES
     *
     * Number of samples. Possible values are 1 or 3.
     */
    std::list<xs::Integer> numberOfSamples;
};

}
}
}
