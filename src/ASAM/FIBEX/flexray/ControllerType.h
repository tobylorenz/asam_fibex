/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "../platform.h"

#include <list>
#include <string>
#include <xercesc/dom/DOM.hpp>

#include "../fx/ControllerType.h"

#include "asam_fibex_export.h"

namespace ASAM {
namespace FIBEX {
namespace flexray {

/**
 * @brief complexType CONTROLLER-TYPE
 *
 * Content model for the entity CONTROLLER with platform specific extendability.
 */
class ASAM_FIBEX_EXPORT ControllerType : public fx::ControllerType
{
public:
    ControllerType();

    /** load from XML DOM element */
    void load(xercesc::DOMElement * element);

    /**
     * @brief KEY-SLOT-USAGE
     */
    enum class KeySlotUsage {
        /** STARTUP-SYNC */
        StartupSync,

        /** SYNC */
        Sync,

        /** NONE */
        None
    } keySlotUsage;

    /** on STARTUP-SYNC or or SYNC */
    unsigned short keySlotId;

    /**
     * @brief MAX-DYNAMIC-PAYLOAD-LENGTH
     *
     * Maximum payload length of a dynamic frame in 16bit WORDS (pPayloadLengthDynMax)
     */
    unsigned short maxDynamicPayloadLength;

    /**
     * @brief CLUSTER-DRIFT-DAMPING
     *
     * Local cluster drift damping factor used for rate correction. Unit:µT (pClusterDriftDamping)
     */
    unsigned short clusterDriftDamping;

    /**
     * @brief DECODING-CORRECTION
     *
     * Value used by the receiver to calculate the difference between primary time reference point and secondary time reference point.
     * Unit: µT (pDecodingCorrection)
     */
    unsigned short decodingCorrection;

    /**
     * @brief LISTEN-TIMEOUT
     *
     * Upper limit for the start up listen timeout and wake up listen timeout.
     * pdListenTimeout[µT] = 2 * (pMicroPerCycle[µT] + pdMaxDrift[µT]).
     * Unit:µT (pdListenTimeout)
     */
    unsigned int listenTimeout;

    /**
     * @brief MAX-DRIFT
     *
     * Maximum drift offset between two nodes that operate with unsynchronized clocks over one communication cycle.
     * Unit: µT (pdMaxDrift)
     */
    unsigned short maxDrift;

    /**
     * @brief EXTERN-OFFSET-CORRECTION
     *
     * Number of microticks added or subtracted to the NIT to carry out a host-requested external offset correction.
     * (pExternOffsetCorrection)
     */
    unsigned short externOffsetCorrection;

    /**
     * @brief EXTERN-RATE-CORRECTION
     *
     * Number of microticks added or subtracted to the cycle to carry out a host-requested external rate correction.
     * (pExternRateCorrection)
     */
    unsigned short externRateCorrection;

    /**
     * @brief LATEST-TX
     *
     * Number of the last minislot in which a frame transmission can start in the dynamic segment.
     * Unit:minislot
     * (pLatestTx)
     */
    unsigned short latestTx;

    /**
     * @brief MICRO-PER-CYCLE
     *
     * Nominal number of microticks in the communication cycle of the local node.
     * If nodes have different microtick durations this number will differ from node to node.
     * (pMicroPerCycle)
     */
    unsigned int microPerCycle;

    /**
     * @brief OFFSET-CORRECTION-OUT
     *
     * Magnitude of the maximum permissible offset correction value.
     * Unit:microtick
     * (pOffsetCorrectionOut)
     */
    unsigned short offsetCorrectionOut;

    /**
     * @brief RATE-CORRECTION-OUT
     *
     * Magnitude of the maximum permissible rate correction value.
     * Unit:µT
     * (pRateCorrectionOut)
     */
    unsigned short rateCorrectionOut;

    /**
     * @brief SAMPLES-PER-MICROTICK
     *
     * Number of samples per microtick (Shall not be part of the conformance test due to implementation dependency).
     * pSamplesPerMicrotick = pdMicrotick / gdSampleClockPeriod = pdMicrotick / (gdBit / cSamplesPerBit) = 8 * pdMicrotick / gdBit;
     * (pSamplesPerMicrotick)
     */
    unsigned short samplesPerMicrotick;

    /**
     * @brief DELAY-COMPENSATION-A
     *
     * Value used to compensate for reception delays on channel A.
     * This covers assumed propagation delay up to cPropagationDelayMax for microticks in the range of 0.0125 µs to 0.05 µs.
     * In practice, the minimum of the propagation delays of all sync nodes should be applied.
     * Unit:microticks
     * Unit:µT
     * (pDelayCompensation[A])
     */
    unsigned short delayCompensationA;

    /**
     * @brief DELAY-COMPENSATION-B
     *
     * Value used to compensate for reception delays on channel B.
     * This covers assumed propagation delay up to cPropagationDelayMax for microticks in the range of 0.0125 µs to 0.05 µs.
     * In practice, the minimum of the propagation delays of all sync nodes should be applied.
     * Unit:microticks
     * Unit:µT
     * (pDelayCompensation[B])
     */
    unsigned short delayCompensationB;

    /**
     * @brief WAKE-UP-PATTERN
     *
     * Number of repetitions of the wakeup symbol that are combined to form a wakeup pattern when the node enters the POC:wakeup send state.
     * (pWakeupPattern)
     */
    unsigned short wakeUpPattern;

    /**
     * @brief ALLOW-HALT-DUE-TO-CLOCK
     *
     * Boolean flag that controls the transition to the POC:halt state due to a clock synchronization errors If set to true, the CC is allowed to transition to POC:halt.
     * If set to false, the CC will not transition to the POC:halt state but will enter or remain in the POC:normal passive state (self healing would still be possible).
     * (pAllowHaltDueToClock)
     */
    bool allowHaltDueToClock;

    /**
     * @brief ALLOW-PASSIVE-TO-ACTIVE
     *
     * Number of consecutive even/odd cycle pairs that must have valid clock correction terms before the CC will be allowed to transition from the POC:normal passive state to POC:normal active state.
     * If set to 0, the CC is not allowed to transition from POC:normal passive to POC:normal active.
     * (pAllowPassiveToActive)
     */
    unsigned short allowPassiveToActive;

    /**
     * @brief ACCEPTED-STARTUP-RANGE
     *
     * Expanded range of measured clock deviation allowed for startup frames during integration.
     * Unit:µT
     * (pdAcceptedStartupRange)
     */
    unsigned short acceptedStartupRange;

    /**
     * @brief MACRO-INITIAL-OFFSET-A
     *
     * Integer number of macroticks between the static slot boundary and the closest macrotick boundary of the secondary time reference point based on the nominal macrotick duration.
     * (pMacroInitialOffset)
     */
    unsigned short macroInitialOffsetA;

    /**
     * @brief MACRO-INITIAL-OFFSET-B
     *
     * Integer number of macroticks between the static slot boundary and the closest macrotick boundary of the secondary time reference point based on the nominal macrotick duration.
     * (pMacroInitialOffset)
     */
    unsigned short macroInitialOffsetB;

    /**
     * @brief MICRO-INITIAL-OFFSET-A
     *
     * Number of microticks between the closest macrotick boundary described by gMacroInitialOffset and the secondary time reference point.
     * The parameter depends on pDelayCompensation[x] and therefor it has to be set independently for each channel.
     * (pMicroInitialOffset[A])
     */
    unsigned short microInitialOffsetA;

    /**
     * @brief MICRO-INITIAL-OFFSET-B
     *
     * Number of microticks between the closest macrotick boundary described by gMacroInitialOffset and the secondary time reference point.
     * The parameter depends on pDelayCompensation[x] and therefor it has to be set independently for each channel.
     * (pMicroInitialOffset[B])
     */
    unsigned short microInitialOffsetB;

    /**
     * @brief SINGLE-SLOT-ENABLED
     *
     * Flag indicating whether or not the node shall enter single slot mode following startup.
     * (pSingleSlotEnabled)
     */
    bool singleSlotEnabled;

    /** MICROTICK */
    float microtick;

    /** MICRO-PER-MACRO-NOM */
    float microPerMacroNom;
};

}
}
}
