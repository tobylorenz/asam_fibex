/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "ControllerType.h"

namespace ASAM {
namespace FIBEX {
namespace flexray {

ControllerType::ControllerType() :
    fx::ControllerType()
{
}

void ControllerType::load(xercesc::DOMElement * element)
{
    fx::ControllerType::load(element);

    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());
        std::string value = xercesc::XMLString::transcode(childElement->getTextContent());

        /* keySlotUsage */
        if (nodeName == "flexray:KEY-SLOT-USAGE") {
            xercesc::DOMElement * childElement2 = childElement->getFirstElementChild();
            std::string nodeName2 = xercesc::XMLString::transcode(childElement2->getNodeName());

            /* startupSync */
            if (nodeName2 == "flexray:STARTUP-SYNC") {
                keySlotUsage = KeySlotUsage::StartupSync;
                std::string value2 = xercesc::XMLString::transcode(childElement2->getTextContent());
                keySlotId = stoul(value2);
            } else

            /* sync */
            if (nodeName2 == "flexray:SYNC") {
                keySlotUsage = KeySlotUsage::Sync;
                std::string value2 = xercesc::XMLString::transcode(childElement2->getTextContent());
                keySlotId = stoul(value2);
            } else

            /* none */
            if (nodeName2 == "flexray:NONE") {
                keySlotUsage = KeySlotUsage::None;
            }
        } else

        /* maxDynamicPayloadLength */
        if (nodeName == "flexray:MAX-DYNAMIC-PAYLOAD-LENGTH") {
            maxDynamicPayloadLength = stoul(value);
        } else

        /* clusterDriftDamping */
        if (nodeName == "flexray:CLUSTER-DRIFT-DAMPING") {
            clusterDriftDamping = stoul(value);
        } else

        /* decodingCorrection */
        if (nodeName == "flexray:DECODING-CORRECTION") {
            decodingCorrection = stoul(value);
        } else

        /* listenTimeout */
        if (nodeName == "flexray:LISTEN-TIMEOUT") {
            listenTimeout = stoul(value);
        } else

        /* maxDrift */
        if (nodeName == "flexray:MAX-DRIFT") {
            maxDrift = stoul(value);
        } else

        /* externOffsetCorrection */
        if (nodeName == "flexray:EXTERN-OFFSET-CORRECTION") {
            externOffsetCorrection = stoul(value);
        } else

        /* externRateCorrection */
        if (nodeName == "flexray:EXTERN-RATE-CORRECTION") {
            externRateCorrection = stoul(value);
        } else

        /* latestTx */
        if (nodeName == "flexray:LATEST-TX") {
            latestTx = stoul(value);
        } else

        /* microPerCycle */
        if (nodeName == "flexray:MICRO-PER-CYCLE") {
            microPerCycle = stoul(value);
        } else

        /* offsetCorrectionOut */
        if (nodeName == "flexray:OFFSET-CORRECTION-OUT") {
            offsetCorrectionOut = stoul(value);
        } else

        /* rateCorrectionOut */
        if (nodeName == "flexray:RATE-CORRECTION-OUT") {
            rateCorrectionOut = stoul(value);
        } else

        /* samplesPerMicrotick */
        if (nodeName == "flexray:SAMPLES-PER-MICROTICK") {
            samplesPerMicrotick = stoul(value);
        } else

        /* delayCompensationA */
        if (nodeName == "flexray:DELAY-COMPENSATION-A") {
            delayCompensationA = stoul(value);
        } else

        /* delayCompensationB */
        if (nodeName == "flexray:DELAY-COMPENSATION-B") {
            delayCompensationB = stoul(value);
        } else

        /* wakeUpPattern */
        if (nodeName == "flexray:WAKE-UP-PATTERN") {
            wakeUpPattern = stoul(value);
        } else

        /* allowHaltDueToClock */
        if (nodeName == "flexray:ALLOW-HALT-DUE-TO-CLOCK") {
            allowHaltDueToClock = (value == "true");
        } else

        /* allowPassiveToActive */
        if (nodeName == "flexray:ALLOW-PASSIVE-TO-ACTIVE") {
            allowPassiveToActive = stoul(value);
        } else

        /* acceptedStartupRange */
        if (nodeName == "flexray:ACCEPTED-STARTUP-RANGE") {
            acceptedStartupRange = stoul(value);
        } else

        /* macroInitialOffsetA */
        if (nodeName == "flexray:MACRO-INITIAL-OFFSET-A") {
            macroInitialOffsetA = stoul(value);
        } else

        /* macroInitialOffsetB */
        if (nodeName == "flexray:MACRO-INITIAL-OFFSET-B") {
            macroInitialOffsetB = stoul(value);
        } else

        /* microInitialOffsetA */
        if (nodeName == "flexray:MICRO-INITIAL-OFFSET-A") {
            microInitialOffsetA = stoul(value);
        } else

        /* microInitialOffsetB */
        if (nodeName == "flexray:MICRO-INITIAL-OFFSET-B") {
            microInitialOffsetB = stoul(value);
        } else

        /* singleSlotEnabled */
        if (nodeName == "flexray:SINGLE-SLOT-ENABLED") {
            singleSlotEnabled = (value == "true");
        } else

        /* microtick */
        if (nodeName == "flexray:MICROTICK") {
            microtick = stof(value);
        } else

        /* microPerMacroNom */
        if (nodeName == "flexray:MICRO-PER-MACRO-NOM") {
            microPerMacroNom = stof(value);
        }
    }
}

}
}
}
