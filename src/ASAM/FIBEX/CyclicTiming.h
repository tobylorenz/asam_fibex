/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "platform.h"

#include <string>
#include <xercesc/dom/DOM.hpp>

#include "asam_fibex_export.h"

namespace ASAM {
namespace FIBEX {

/**
 * CYCLIC-TIMING
 */
class ASAM_FIBEX_EXPORT CyclicTiming
{
public:
    CyclicTiming();

    /** load from XML DOM element */
    bool load(xercesc::DOMElement * element);

    /** fx */
    struct Fx {
        Fx();

        /** REPEATING-TIME-RANGE */
        struct RepeatingTimeRange {
            RepeatingTimeRange();

            /** VALUE */
            std::string value;
        } repeatingTimeRange;

        /** STARTING-TIME-RANGE */
        struct StartingTimeRange {
            StartingTimeRange();

            /** VALUE */
            std::string value;
        } startingTimeRange;

        /** START-CONDITION */
        struct StartCondition {
            StartCondition();

            /** SIGNAL-TRIGGER */
            struct SignalTrigger {
                SignalTrigger();

                /** SIGNAL-STATE */
                enum class SignalState {
                    /** VALUE_CHANGED */
                    ValueChanged,

                    /** VALUE_NOT_CHANGED */
                    ValueNotChanged,

                    /** VALUE_DEFAULT */
                    ValueDefault,

                    /** VALUE_NOT_DEFAULT */
                    ValueNotDefault,

                    /** OTHER */
                    Other
                } signalState;

                /** SIGNAL-INSTANCE-REF */
                std::string signalInstanceRef;
            } signalTrigger;
        } startCondition;

        /** STOP-CONDITION */
        struct StopCondition {
            StopCondition();

            /** SIGNAL-TRIGGER */
            struct SignalTrigger {
                SignalTrigger();

                /** SIGNAL-STATE */
                enum class SignalState {
                    /** VALUE_CHANGED */
                    ValueChanged,

                    /** VALUE_NOT_CHANGED */
                    ValueNotChanged,

                    /** VALUE_DEFAULT */
                    ValueDefault,

                    /** VALUE_NOT_DEFAULT */
                    ValueNotDefault,

                    /** OTHER */
                    Other
                } signalState;

                /** SIGNAL-INSTANCE-REF */
                std::string signalInstanceRef;
            } signalTrigger;
        } stopCondition;

        /** FINAL-REPETITIONS */
        unsigned int finalRepetitions;
    } fx;

    /** ho */
    struct Ho {
        Ho();

        /** DESC */
        std::string desc;
    } ho;
};

}
}
