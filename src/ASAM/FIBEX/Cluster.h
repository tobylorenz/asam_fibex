/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "platform.h"

#include <list>
#include <string>
#include <xercesc/dom/DOM.hpp>

#include "ho/NameDetails.h"

#include "asam_fibex_export.h"

namespace ASAM {
namespace FIBEX {

/**
 * @brief fx:CLUSTER-type
 *
 * Content model for the entity CLUSTER with platform specific extendability.
 */
class ASAM_FIBEX_EXPORT Cluster : public ho::NameDetails
{
public:
    Cluster();

    /** load from XML DOM element */
    bool load(xercesc::DOMElement * element);

    /** ID */
    std::string id;

    /** xsi:type */
    std::string xsiType;

    /** fx */
    struct Fx {
        Fx();

        /**
         * @brief SPEED
         *
         * A channels speed in bits per second
         */
        unsigned long speed;

        /**
         * @brief IS-HIGH-LOW-BIT-ORDER
         *
         * Indicates whether the significance of the bits (in a byte) is
         * decreasing (value=true) or increasing (value=false) with the time.
         * All frames on this cluster have to be encoded in this bit order.
         * Usually the bit order is fixed with the protocol specification.
         */
        bool isHighLowBitOrder;

        /**
         * @brief BIT-COUNTING-POLICY
         *
         * Two counting policies have been established. We will call them
         * monotone and sawtooth counting. While monotone counting is strictly
         * geared to the time axis, sawtooth counting inside of a byte is in
         * diametrical opposition to the time. The bits of all frames on this
         * cluster are counted in this way.
         */
        enum class BitCountingPolicy {
            Monotone,
            Sawtooth
        } bitCountingPolicy;

        /**
         * @brief PROTOCOL
         *
         * A clusters protocol
         */
        std::string protocol;

        /**
         * @brief PROTOCOL-VERSION
         *
         * Version specifier for a communication protocol
         */
        std::string protocolVersion;

        /**
         * @brief PHYSICAL
         *
         * A clusters physical layer
         */
        std::string physical;

        /**
         * @brief PHYSICAL-VERSION
         *
         * Version specifier for a electrical physical layer
         */
        std::string physicalVersion;

        /**
         * @brief CHANNEL-REFS
         *
         * Collection of references (foreign keys)
         */
        std::list<std::string> channelRefs;

        /**
         * @brief MEDIUM
         *
         * A clusters medium type
         */
        enum class Medium {
            Electrical,
            Optical,
            Other
        } medium;

        /**
         * @brief NUMBER-OF-CYCLES
         *
         * Total number of cycles until a temporal transmission pattern is
         * repeated. The CYCLE-COUNTER of an ABSOLUTELY-SCHEDULED-TIMING is
         * evaluated against this parameter.
         */
        unsigned short numberOfCycles;

        /**
         * @brief MAX-FRAME-LENGTH
         *
         * Maximal supported length in bytes for frames in this cluster.
         */
        unsigned int maxFrameLength;
    } fx;

    /** flexray */
    struct Flexray {
        Flexray();

        /** COLD-START-ATTEMPTS */
        unsigned short coldStartAttempts;

        /** ACTION-POINT-OFFSET */
        unsigned short actionPointOffset;

        /** DYNAMIC-SLOT-IDLE-PHASE */
        unsigned short dynamicSlotIdlePhase;

        /** MINISLOT */
        unsigned short minislot;

        /** MINISLOT-ACTION-POINT-OFFSET */
        unsigned short minislotActionPointOffset;

        /** N-I-T */
        unsigned short nit;

        /** SAMPLE-CLOCK-PERIOD */
        float sampleClockPeriod;

        /** STATIC-SLOT */
        unsigned short staticSlot;

        /** SYMBOL-WINDOW */
        unsigned short symbolWindow;

        /** T-S-S-TRANSMITTER */
        unsigned short tssTransmitter;

        /** WAKE-UP */
        struct WakeUp {
            WakeUp();

            /** WAKE-UP-SYMBOL-RX-IDLE */
            unsigned short wakeUpSymbolRxIdle;

            /** WAKE-UP-SYMBOL-RX-LOW */
            unsigned short wakeUpSymbolRxLow;

            /** WAKE-UP-SYMBOL-RX-WINDOW */
            unsigned short wakeUpSymbolRxWindow;

            /** WAKE-UP-SYMBOL-TX-IDLE */
            unsigned short wakeUpSymbolTxIdle;

            /** WAKE-UP-SYMBOL-TX-LOW */
            unsigned short wakeUpSymbolTxLow;
        } wakeUp;

        /** LISTEN-NOISE */
        unsigned short listenNoise;

        /** MACRO-PER-CYCLE */
        unsigned short macroPerCycle;

        /** MACROTICK */
        float macrotick;

        /** MAX-INITIALIZATION-ERROR */
        float maxInitializationError;

        /** MAX-WITHOUT-CLOCK-CORRECTION-FATAL */
        unsigned short maxWithoutClockCorrectionFatal;

        /** MAX-WITHOUT-CLOCK-CORRECTION-PASSIVE */
        unsigned short maxWithoutClockCorrectionPassive;

        /** NETWORK-MANAGEMENT-VECTOR-LENGTH */
        unsigned short networkManagementVectorLength;

        /** NUMBER-OF-MINISLOTS */
        unsigned short numberOfMinislots;

        /** NUMBER-OF-STATIC-SLOTS */
        unsigned short numberOfStaticSlots;

        /** OFFSET-CORRECTION-START */
        unsigned short offsetCorrectionStart;

        /** PAYLOAD-LENGTH-STATIC */
        unsigned short payloadLengthStatic;

        /** SYNC-NODE-MAX */
        unsigned short syncNodeMax;

        /** CAS-RX-LOW-MAX */
        unsigned short casRxLowMax;

        /** BIT */
        float bit;

        /** CYCLE */
        unsigned short cycle;

        /** CLUSTER-DRIFT-DAMPING */
        unsigned short clusterDriftDamping;
    } flexray;
};

}
}
