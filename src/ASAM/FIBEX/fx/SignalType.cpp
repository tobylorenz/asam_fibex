/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "SignalType.h"

#include <iostream>

namespace ASAM {
namespace FIBEX {
namespace fx {

SignalType::SignalType() :
    fx::RevisedElementType(),
    defaultValue(),
    codingRef(),
    type(),
    priority()
{
}

void SignalType::load(xercesc::DOMElement * element)
{
    fx::RevisedElementType::load(element);

    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());
        std::string value = xercesc::XMLString::transcode(childElement->getTextContent());

        /* defaultValue */
        if (nodeName == "fx:DEFAULT-VALUE") {
            defaultValue.push_back(stod(value));
        } else

        /* codingRef */
        if (nodeName == "fx:CODING-REF") {
            std::string id2 = xercesc::XMLString::transcode(childElement->getAttribute(xercesc::XMLString::transcode("ID-REF")));
            codingRef = id2;
        } else

        /* signalType */
        if (nodeName == "fx:SIGNAL-TYPE") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string nodeName2 = xercesc::XMLString::transcode(childElement2->getNodeName());
                std::string value2 = xercesc::XMLString::transcode(childElement2->getTextContent());

                /* type */
                if (nodeName2 == "fx:TYPE") {
                    if (value2 == "RESPONSE-ERROR") {
                        type = Type::ResponseError;
                    } else
                    if (value2 == "REQUEST") {
                        type = Type::Request;
                    } else
                    if (value2 == "ALIVE-COUNTER") {
                        type = Type::AliveCounter;
                    } else
                    if (value2 == "CHECKSUM") {
                        type = Type::Checksum;
                    } else
                    if (value2 == "OTHER") {
                        type = Type::Other;
                    } else
                    {
                        std::cerr << "Unrecognized nodeName " << nodeName2 << std::endl;
                    }
                }
            }
        }
    }
}

}
}
}
