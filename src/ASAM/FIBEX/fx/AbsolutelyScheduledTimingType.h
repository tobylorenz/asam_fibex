/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "../platform.h"

#include <list>
#include <xercesc/dom/DOM.hpp>

#include "../ho/Desc.h"
#include "../fx/SlotIdType.h"
#include "../fx/CycleCounterType.h"
#include "../fx/CycleRepetitionType.h"

#include "asam_fibex_export.h"

namespace ASAM {
namespace FIBEX {
namespace fx {

/**
 * @brief complexType ABSOLUTELY-SCHEDULED-TIMING-TYPE
 *
 * Content model for the entity TIMING in the absolutely scheduled peculiarity.
 */
class ASAM_FIBEX_EXPORT AbsolutelyScheduledTimingType
{
public:
    AbsolutelyScheduledTimingType();

    /** load from XML DOM element */
    void load(xercesc::DOMElement * element);

    /** @brief element DESC */
    std::list<ho::Desc> desc;

    /**
     * @brief element SLOT-ID
     *
     * Identifier for the (fixed) slot where the frame described by this timing is sent in the static part of a application cycle.
     * Used as priority for arbitration in the dynamic part of a cycle.
     * Usualy used (with cycleCounter) for frame identification
     */
    fx::SlotIdType slotId;

    /**
     * @brief element CYCLE-COUNTER
     *
     * The communication cycle where the frame described by this timing is sent.
     * If a timing is given in this way the referencing cluster must specify the NUMBER-OF-CYCLES as upper bound and point of total repetition.
     */
    std::list<fx::CycleCounterType> cycleCounter;

    /**
     * @brief element BASE-CYCLE
     *
     * The first communication cycle where the frame described by this timing is sent
     */
    std::list<fx::CycleCounterType> baseCycle;

    /**
     * @brief element CYCLE-REPETITION
     *
     * The number of communication cycles (after the frist cycle) whenever the frame described by this timing is sent again
     */
    std::list<fx::CycleRepetitionType> cycleRepetition;

    /**
     * @brief element MANUFACTURER-EXTENSION
     * @todo to be implemented
     *
     * Reserved space providing schema extendibility.
     */
};

}
}
}
