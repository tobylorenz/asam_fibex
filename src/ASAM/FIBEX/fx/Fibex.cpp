/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Fibex.h"

namespace ASAM {
namespace FIBEX {
namespace fx {

Fibex::Fibex() :
    project(),
    protocols(),
    elements(),
    processingInformation(),
    requirements(),
    version()
{
}

void Fibex::load(xercesc::DOMElement * element)
{
    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());

        /* project */
        if (nodeName == "fx:PROJECT") {
            project.load(childElement);
        } else

        /* protocols */
        if (nodeName == "fx:PROTOCOLS") {
            Protocols p;
            p.load(childElement);
            protocols.push_back(p);
        } else

        /* elements */
        if (nodeName == "fx:ELEMENTS") {
            elements.load(childElement);
        } else

        /* processingInformation */
        if (nodeName == "fx:PROCESSING-INFORMATION") {
            ProcessingInformation pi;
            pi.load(childElement);
            processingInformation.push_back(pi);
        } else

        /* requirements */
        if (nodeName == "fx:REQUIREMENTS") {
            Requirements r;
            r.load(childElement);
            requirements.push_back(r);
        }
    }

    /* version */
    version.assign(xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("VERSION"))));
}

}
}
}
