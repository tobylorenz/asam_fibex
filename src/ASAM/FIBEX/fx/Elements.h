/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "../platform.h"

#include <list>
#include <xercesc/dom/DOM.hpp>

#include "../fx/Extent.h"
#include "ChannelType.h"
#include "ClusterType.h"
#include "CompositeType.h"
#include "EcuType.h"
#include "FrameType.h"
#include "FunctionType.h"
#include "GatewayType.h"
#include "PduType.h"
#include "SignalType.h"

#include "asam_fibex_export.h"

namespace ASAM {
namespace FIBEX {
namespace fx {

/**
 * @brief complexType ELEMENTS
 *
 * Root of all stand-alone elements
 */
class ASAM_FIBEX_EXPORT Elements
{
public:
    Elements();

    /** load from XML DOM element */
    void load(xercesc::DOMElement * element);

    struct Clusters {
        Clusters();

        std::list<fx::Extent> extent;

        /**
         * In multi-channel systems such as TTP or FlexRay known as 'bus'.
         * To avoid confusion, we decides not to use this term any more.
         */
        std::list<ClusterType> cluster;
    };

    /** Top level element of all clusters */
    std::list<Clusters> clusters;

    struct Channels {
        Channels();
        std::list<fx::Extent> extent;

        /**
         * In single-channel systems such as CAN or LIN known as 'bus'.
         * To avoid confusion, we decides not to use this term any more.
         */
        std::list<ChannelType> channel;
    };

    /** Top level element of all channels */
    std::list<Channels> channels;

    struct Gateways {
        Gateways();

        std::list<fx::Extent> extent;

        /**
         * A gateway is a specific ECU translating frames and signals from one channel into another.
         * Further description of gateway behaviour is intended for a later version of FIBEX
         */
        std::list<GatewayType> gateway;
    };

    /** Top level element of all gateways */
    std::list<Gateways> gateways;

    struct Ecus {
        Ecus();

        std::list<fx::Extent> extent;

        /** Electronic Control Unit (sometimes called 'node') */
        std::list<EcuType> ecu;
    };

    /** Top level element of all ECUs */
    std::list<Ecus> ecus;

    struct Pdus {
        Pdus();

        std::list<fx::Extent> extent;

        std::list<PduType> pdu;
    };

    /** Top level element of all PDUs */
    std::list<Pdus> pdus;

    struct Frames {
        Frames();

        std::list<fx::Extent> extent;

        /**
         * Smallest piece of information exchanged over the communication system.
         * A frame normally consists of a header and a data (payload) section.
         * The data section contains signals.
         */
        std::list<FrameType> frame;
    };

    /** Top level element of all frames */
    std::list<Frames> frames;

    struct Functions {
        Functions();

        std::list<fx::Extent> extent;

        /** Part of the application running in an ECU */
        std::list<FunctionType> function;
    };

    /** Top level element of all functions */
    std::list<Functions> functions;

    struct Signals {
        Signals();

        std::list<fx::Extent> extent;

        /** Smallest logical piece of information exchanged by applications */
        std::list<SignalType> signal;
    };

    /** Top level element of all signals */
    std::list<Signals> signals;

    struct Composites {
        Composites();

        std::list<fx::Extent> extent;

        /** A hierarchical structure on functions or ecus. */
        std::list<CompositeType> composite;
    };

    /** Top level element of all composites */
    std::list<Composites> composites;
};

}
}
}
