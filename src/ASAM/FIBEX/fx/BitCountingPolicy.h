/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "../platform.h"

#include <list>
#include <set>
#include <string>
#include <xercesc/dom/DOM.hpp>

#include "asam_fibex_export.h"

namespace ASAM {
namespace FIBEX {
namespace fx {

/**
 * @brief simpleType BIT-COUNTING-POLICY
 *
 * Two counting policies have been established.
 * We will call them monotone and sawtooth counting.
 * While monotone counting is strictly geared to the time axis, sawtooth counting inside of a byte is in diametrical opposition to the time.
 * The bits of all frames on this cluster are counted in this way.
 */
class ASAM_FIBEX_EXPORT BitCountingPolicy
{
public:
    BitCountingPolicy();

    /** load from XML DOM element */
    void load(xercesc::DOMElement * element);

    /** @todo restriction enumeration */
};

}
}
}
