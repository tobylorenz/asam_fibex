/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "../platform.h"

#include <list>
#include <set>
#include <string>
#include <xercesc/dom/DOM.hpp>

#include "asam_fibex_export.h"

namespace ASAM {
namespace FIBEX {
namespace fx {

/**
 * @brief complexType PORT-REQ-TYPE
 *
 * Content model for the entity PORT-REQUIREMENT
 */
class ASAM_FIBEX_EXPORT PortReqType
{
public:
    PortReqType();

    /** load from XML DOM element */
    void load(xercesc::DOMElement * element);

    /** @todo element PORT-REFS */
    /** @todo element CHANNEL-REFS */
    /** @todo element CONTROLLER-REFS */
    /** @todo element MAX-AGE */
    /** @todo element GENERATION-TYPE */
    /** @todo element CYCLE-PERIOD */
    /** @todo element CYCLE-OFFSET */
    /** @todo element REDUNDANCY-LVL */
    /** @todo element MANUFACTURER-EXTENSION */
};

}
}
}
