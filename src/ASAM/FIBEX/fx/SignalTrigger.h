/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "../platform.h"

#include <list>
#include <set>
#include <string>
#include <xercesc/dom/DOM.hpp>

#include "RevisedElementType.h"

#include "asam_fibex_export.h"

namespace ASAM {
namespace FIBEX {
namespace fx {

/**
 * @brief complexType SIGNAL-TRIGGER
 *
 * Signal state that must rule that the referencing timing is executed.
 * To enable the description of combined tirggers the occurence of SIGNAL-TRIGGER as well as of SIGNAL-STATE combined with SIGNAL-INSTANCE-REF is unbounded.
 * Use one trigger with many states, if these states must rule simultaneously (AND related) for execution.
 * Use a separate trigger for each state if only one state needs to rule for execution.
 */
class ASAM_FIBEX_EXPORT SignalTrigger : public RevisedElementType
{
public:
    SignalTrigger();

    /** load from XML DOM element */
    void load(xercesc::DOMElement * element);

    /** @todo element DEFAULT-VALUE */
    /** @todo element CODING-REF */
    /** @todo element SIGNAL-TYPE */
    /** @todo element PRIORITY */
    /** @todo element ECU-SENDING-BEHAVIOR */
    /** @todo element MANUFACTURER-EXTENSION */
};

}
}
}
