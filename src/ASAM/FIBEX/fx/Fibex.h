/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "../platform.h"

#include <list>
#include <xercesc/dom/DOM.hpp>

#include "Elements.h"
#include "ProcessingInformation.h"
#include "ProjectType.h"
#include "Protocols.h"
#include "Requirements.h"
#include "VersionType.h"

#include "asam_fibex_export.h"

namespace ASAM {
namespace FIBEX {
namespace fx {

/**
 * @brief element FIBEX
 *
 * Root element
 */
class ASAM_FIBEX_EXPORT Fibex : public VersionType
{
public:
    Fibex();

    /** load from XML DOM element */
    void load(xercesc::DOMElement * element);

    /**
     * element PROJECT
     *
     * The project/vehicle described in the referencing file
     */
    ProjectType project;

    /** element PROTOCOLS */
    std::list<Protocols> protocols;

    /** element ELEMENTS */
    Elements elements;

    /** element PROCESSING-INFORMATION */
    std::list<ProcessingInformation> processingInformation;

    /** element REQUIREMENTS */
    std::list<Requirements> requirements;

    /**
     * attribute VERSION
     *
     * To identify the schemas version that should be applied on the referencing file.
     * Note that this value is fixed on the current schema version to avoid confusion checking old FIBEX instances against a new FIBEX schema or oposite.
     */
    VersionType version;
};

}
}
}
