/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Elements.h"

namespace ASAM {
namespace FIBEX {
namespace fx {

Elements::Clusters::Clusters() :
    extent(),
    cluster()
{
}

Elements::Channels::Channels() :
    extent(),
    channel()
{
}

Elements::Gateways::Gateways() :
    extent(),
    gateway()
{
}

Elements::Ecus::Ecus() :
    extent(),
    ecu()
{
}

Elements::Pdus::Pdus() :
    extent(),
    pdu()
{
}

Elements::Frames::Frames() :
    extent(),
    frame()
{
}

Elements::Functions::Functions() :
    extent(),
    function()
{
}

Elements::Signals::Signals() :
    extent(),
    signal()
{
}

Elements::Composites::Composites() :
    extent(),
    composite()
{
}

Elements::Elements() :
    clusters(),
    channels(),
    gateways(),
    ecus(),
    pdus(),
    frames(),
    functions(),
    signals(),
    composites()
{
}

void Elements::load(xercesc::DOMElement * element)
{
    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());

        /* clusters */
        if (nodeName == "fx:CLUSTERS") {
            Clusters c1;
            clusters.push_back(c1);
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                ClusterType c2;
                c2.load(childElement2);
                c1.cluster.push_back(c2);
            }
        } else

        /* channels */
        if (nodeName == "fx:CHANNELS") {
            Channels c;
            channels.push_back(c);
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
            }
        } else

        /* gateways */
        if (nodeName == "fx:GATEWAYS") {
            Gateways g;
            gateways.push_back(g);
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
            }
        } else

        /* ecus */
        if (nodeName == "fx:ECUS") {
            Ecus e;
            ecus.push_back(e);
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
            }
        } else

        /* pdus */
        if (nodeName == "fx:PDUS") {
            Pdus p;
            pdus.push_back(p);
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
            }
        } else

        /* frames */
        if (nodeName == "fx:FRAMES") {
            Frames f;
            frames.push_back(f);
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
            }
        } else

        /* functions */
        if (nodeName == "fx:FUNCTIONS") {
            Functions f;
            functions.push_back(f);
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
            }
        } else

        /* signals */
        if (nodeName == "fx:SIGNALS") {
            Signals s;
            signals.push_back(s);
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
            }
        } else

        /* composites */
        if (nodeName == "fx:COMPOSITES") {
            Composites c;
            composites.push_back(c);
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
            }
        }
    }
}

}
}
}
