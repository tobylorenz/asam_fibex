/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "platform.h"

#include <string>
#include <xercesc/dom/DOM.hpp>

#include "asam_fibex_export.h"

namespace ASAM {
namespace FIBEX {

/**
 * ABSOLUTELY-SCHEDULED-TIMING
 */
class ASAM_FIBEX_EXPORT AbsolutelyScheduledTiming
{
public:
    AbsolutelyScheduledTiming();

    /** load from XML DOM element */
    bool load(xercesc::DOMElement * element);

    /** fx */
    struct Fx {
        Fx();

        /** SLOT-ID */
        unsigned int slotId;

        /** BASE-CYCLE */
        unsigned int baseCycle;

        /** CYCLE-REPETITION */
        unsigned int cycleRepetition;
    } fx;
};

}
}
