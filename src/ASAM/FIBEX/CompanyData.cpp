/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "CompanyData.h"

namespace ASAM {
namespace FIBEX {

CompanyData::CompanyData() :
    ho::NameDetails(),
    id(),
    role()
{
}

bool CompanyData::load(xercesc::DOMElement * element)
{
    ho::NameDetails::load(element);

    /* read attributes */
    id = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("ID")));
    std::string roleStr = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("ROLE")));
    if (roleStr == "MANUFACTURER") {
        role = Role::Manufacturer;
    } else

    if (roleStr == "SUPPLIER") {
        role = Role::Supplier;
    } else

    {
            std::string nodeName = xercesc::XMLString::transcode(element->getNodeName());
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
    }

    return true;
}

}
}
