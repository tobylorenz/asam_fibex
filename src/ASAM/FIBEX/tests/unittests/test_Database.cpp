#define BOOST_TEST_MODULE Database
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <boost/filesystem.hpp>

#include "ASAM/FIBEX/Fibex.h"

void progressCallback(float numerator, float denominator)
{
    std::cout << "Progress: filepos=" << numerator << " percent="<< std::fixed << 100 * (numerator / denominator) << '%' << std::endl;
}

BOOST_AUTO_TEST_CASE(Database)
{
    ASAM::FIBEX::Fibex fibex;
    fibex.setProgressCallback(&progressCallback);

    /* load database */
    boost::filesystem::path infile(CMAKE_CURRENT_SOURCE_DIR "/data/Database.xml");
    std::string filename = infile.string();
    std::cout << "Using database " << filename << std::endl;
    BOOST_CHECK(fibex.load(filename));

    /* projects */
    std::cout << "projects:" << std::endl;
    std::cout << "  " << fibex.fx.project.id << std::endl;

    /* clusters */
    std::cout << "clusters:" << std::endl;
    for (std::map<std::string, ASAM::FIBEX::Cluster>::iterator cluster = fibex.fx.elements.fx.clusters.begin(); cluster != fibex.fx.elements.fx.clusters.end(); ++cluster) {
        std::cout << "  " << cluster->second.id << std::endl;
    }

    /* channels */
    std::cout << "channels:" << std::endl;
    for (std::map<std::string, ASAM::FIBEX::Channel>::iterator channel = fibex.fx.elements.fx.channels.begin(); channel != fibex.fx.elements.fx.channels.end(); ++channel) {
        std::cout << "  " << channel->second.id << std::endl;
    }

    /* ecus */
    std::cout << "ecus:" << std::endl;
    for (std::map<std::string, ASAM::FIBEX::Ecu>::iterator ecu = fibex.fx.elements.fx.ecus.begin(); ecu != fibex.fx.elements.fx.ecus.end(); ++ecu) {
        std::cout << "  " << ecu->second.id << std::endl;
    }

    /* pdus */
    std::cout << "pdus:" << std::endl;
    for (std::map<std::string, ASAM::FIBEX::Pdu>::iterator pdu = fibex.fx.elements.fx.pdus.begin(); pdu != fibex.fx.elements.fx.pdus.end(); ++pdu) {
        std::cout << "  " << pdu->second.id << std::endl;
    }

    /* frames */
    std::cout << "frames:" << std::endl;
    for (std::map<std::string, ASAM::FIBEX::Frame>::iterator frame = fibex.fx.elements.fx.frames.begin(); frame != fibex.fx.elements.fx.frames.end(); ++frame) {
        std::cout << "  " << frame->second.id << std::endl;
    }

    /* signals */
    std::cout << "signals:" << std::endl;
    for (std::map<std::string, ASAM::FIBEX::Signal>::iterator signal = fibex.fx.elements.fx.signals.begin(); signal != fibex.fx.elements.fx.signals.end(); ++signal) {
        std::cout << "  " << signal->second.id << std::endl;
    }
}
