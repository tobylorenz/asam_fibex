/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "CompuScale.h"

namespace ASAM {
namespace FIBEX {

CompuScale::LowerLimit::LowerLimit() :
    intervalType(IntervalType::Closed),
    value()
{
}

CompuScale::UpperLimit::UpperLimit() :
    intervalType(IntervalType::Closed),
    value()
{
}

CompuScale::CompuConst::CompuConst() :
    vt()
{
}

CompuScale::CompuRationalCoeffs::CompuNumerator::CompuNumerator() :
    v()
{
}

CompuScale::CompuRationalCoeffs::CompuRationalCoeffs() :
    compuNumerator()
{
}

CompuScale::CompuScale() :
    lowerLimit(),
    upperLimit(),
    compuConst(),
    compuRationalCoeffs()
{
}

bool CompuScale::load(xercesc::DOMElement * element)
{
    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());

        /* lowerLimit */
        if (nodeName == "ho:LOWER-LIMIT") {
            /* intervalType */
            std::string intervalType = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("INTERVAL-TYPE")));
            if (!intervalType.empty()) {
                if (intervalType == "OPEN") {
                    lowerLimit.intervalType = LowerLimit::IntervalType::Open;
                } else
                if (intervalType == "CLOSED") {
                    lowerLimit.intervalType = LowerLimit::IntervalType::Closed;
                } else
                if (intervalType == "INFINITE") {
                    lowerLimit.intervalType = LowerLimit::IntervalType::Infinite;
                } else
                {
                    std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
                }
            }

            /* value */
            std::string value = xercesc::XMLString::transcode(childElement->getTextContent());
            lowerLimit.value = stod(value);
        } else

        /* upperLimit */
        if (nodeName == "ho:UPPER-LIMIT") {
            /* intervalType */
            std::string intervalType = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("INTERVAL-TYPE")));
            if (!intervalType.empty()) {
                if (intervalType == "OPEN") {
                    upperLimit.intervalType = UpperLimit::IntervalType::Open;
                } else
                if (intervalType == "CLOSED") {
                    upperLimit.intervalType = UpperLimit::IntervalType::Closed;
                } else
                if (intervalType == "INFINITE") {
                    upperLimit.intervalType = UpperLimit::IntervalType::Infinite;
                } else
                {
                    std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
                }
            }

            /* value */
            std::string value = xercesc::XMLString::transcode(childElement->getTextContent());
            upperLimit.value = stod(value);
        } else

        /* compuConst */
        if (nodeName == "ho:COMPU-CONST") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string nodeName2 = xercesc::XMLString::transcode(childElement2->getNodeName());
                if (nodeName2 == "ho:VT") {
                    std::string value = xercesc::XMLString::transcode(childElement2->getTextContent());
                    compuConst.vt = value;
                } else
                {
                    std::cerr << "Unrecognized nodeName " << nodeName2 << std::endl;
                }
            }
        } else

        /* compuRationalCoeffs */
        if (nodeName == "ho:COMPU-RATIONAL-COEFFS") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string nodeName2 = xercesc::XMLString::transcode(childElement2->getNodeName());
                if (nodeName2 == "ho:COMPU-NUMERATOR") {
                    for (xercesc::DOMElement * childElement3 = childElement2->getFirstElementChild(); childElement3 != nullptr; childElement3 = childElement3->getNextElementSibling()) {
                        std::string nodeName3 = xercesc::XMLString::transcode(childElement3->getNodeName());
                        if (nodeName3 == "ho:V") {
                            std::string value3 = xercesc::XMLString::transcode(childElement3->getTextContent());
                            compuRationalCoeffs.compuNumerator.v.push_back(stoul(value3));
                        } else
                        {
                            std::cerr << "Unrecognized nodeName " << nodeName2 << std::endl;
                        }
                    }
                } else
                {
                    std::cerr << "Unrecognized nodeName " << nodeName2 << std::endl;
                }
            }
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }

    return true;
}

}
}
