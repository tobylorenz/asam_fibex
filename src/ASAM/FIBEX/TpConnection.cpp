/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "ho/NameDetails.h"
#include "TpConnection.h"

namespace ASAM {
namespace FIBEX {

TpConnection::Asr30tp::Asr30tp() :
    tpPduUsageRefs(),
    tpNodeTxRefs(),
    tpNodeRxRefs()
{
}

TpConnection::TpConnection() :
    ho::NameDetails(),
    id(),
    asr30tp()
{
}

bool TpConnection::load(xercesc::DOMElement * element)
{
    ho::NameDetails::load(element);

    /* read attributes */
    id = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("ID")));

    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());
        std::string value = xercesc::XMLString::transcode(childElement->getTextContent());

        /* tpPduUsageRefs */
        if (nodeName == "asr30tp:TP-PDU-USAGE-REFS") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string id2 = xercesc::XMLString::transcode(childElement2->getAttribute(xercesc::XMLString::transcode("ID-REF")));
                asr30tp.tpPduUsageRefs.insert(id2);
            }
        } else

        /* tpNodeTxRefs */
        if (nodeName == "asr30tp:TP-NODE-TX-REFS") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string id2 = xercesc::XMLString::transcode(childElement2->getAttribute(xercesc::XMLString::transcode("ID-REF")));
                asr30tp.tpNodeTxRefs.insert(id2);
            }
        } else

        /* tpNodeRxRefs */
        if (nodeName == "asr30tp:TP-NODE-RX-REFS") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string id2 = xercesc::XMLString::transcode(childElement2->getAttribute(xercesc::XMLString::transcode("ID-REF")));
                asr30tp.tpNodeRxRefs.insert(id2);
            }
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }

    return true;
}

}
}
