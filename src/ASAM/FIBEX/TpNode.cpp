/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "TpNode.h"

namespace ASAM {
namespace FIBEX {

TpNode::Fx::TpAddressRef::TpAddressRef() :
    addressing(),
    idRef()
{
}

TpNode::Fx::Fx() :
    tpAddressRef()
{
}

TpNode::Asr30tp::Asr30tp() :
    stmin(),
    timeoutAr(),
    timeoutAs(),
    connectorRefs()
{
}

TpNode::Asr30Frtp::Asr30Frtp() :
    bufferRequest(),
    maxAr(),
    maxAs(),
    maxFrif(),
    timeBuffer(),
    timeFrif()
{
}

TpNode::TpNode() :
    ho::NameDetails(),
    id(),
    xsiType(),
    asr30tp(),
    fx(),
    asr30frtp()
{
}

bool TpNode::load(xercesc::DOMElement * element)
{
    ho::NameDetails::load(element);

    /* read attributes */
    id = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("ID")));
    xsiType = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("xsi:type")));

    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());
        std::string value = xercesc::XMLString::transcode(childElement->getTextContent());

        /* stmin */
        if (nodeName == "asr30tp:STMIN") {
            asr30tp.stmin = stoul(value);
        } else

        /* timeoutAr */
        if (nodeName == "asr30tp:TIMEOUT-AR") {
            asr30tp.timeoutAr = value;
        } else

        /* timeoutAs */
        if (nodeName == "asr30tp:TIMEOUT-AS") {
            asr30tp.timeoutAs = value;
        } else

        /* connectorRefs */
        if (nodeName == "asr30tp:CONNECTOR-REFS") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string id2 = xercesc::XMLString::transcode(childElement2->getAttribute(xercesc::XMLString::transcode("ID-REF")));
                asr30tp.connectorRefs.insert(id2);
            }
        } else

        /* tpAddressRef */
        if (nodeName == "fx:TP-ADDRESS-REF") {
            std::string addressing = xercesc::XMLString::transcode(childElement->getAttribute(xercesc::XMLString::transcode("ADDRESSING")));
            std::string idRef = xercesc::XMLString::transcode(childElement->getAttribute(xercesc::XMLString::transcode("ID-REF")));
            if (addressing == "PHYSICAL") {
                fx.tpAddressRef.addressing = Fx::TpAddressRef::Addressing::Physical;
            } else

            if (addressing == "FUNCTIONAL") {
                fx.tpAddressRef.addressing = Fx::TpAddressRef::Addressing::Functional;
            } else

            if (addressing == "TESTER") {
                fx.tpAddressRef.addressing = Fx::TpAddressRef::Addressing::Tester;
            } else

            if (addressing == "OTHER") {
                fx.tpAddressRef.addressing = Fx::TpAddressRef::Addressing::Other;
            } else

            {
                std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
            }

            fx.tpAddressRef.idRef = idRef;
        } else

        /* bufferRequest */
        if (nodeName == "asr30frtp:BUFFER-REQUEST") {
            asr30frtp.bufferRequest = stoul(value);
        } else

        /* maxAr */
        if (nodeName == "asr30frtp:MAX-AR") {
            asr30frtp.maxAr = stoul(value);
        } else

        /* maxAs */
        if (nodeName == "asr30frtp:MAX-AS") {
            asr30frtp.maxAs = stoul(value);
        } else

        /* maxFrif */
        if (nodeName == "asr30frtp:MAX-FRIF") {
            asr30frtp.maxFrif = stoul(value);
        } else

        /* timeBuffer */
        if (nodeName == "asr30frtp:TIME-BUFFER") {
            asr30frtp.timeBuffer = value;
        } else

        /* timeFrif */
        if (nodeName == "asr30frtp:TIME-FRIF") {
            asr30frtp.timeFrif = value;
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }

    return true;
}

}
}
