/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Fibex.h"

#include <fstream>
#include <iostream>
#include <string>

#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>

namespace ASAM {
namespace FIBEX {

Fibex::Fibex() :
    fx::Fibex(),
    domParser(nullptr)
{
    xercesc::XMLPlatformUtils::Initialize();

    /* Xerces DOM Parser */
    domParser = new xercesc::XercesDOMParser();
    domParser->setValidationScheme(xercesc::XercesDOMParser::Val_Always);
    domParser->setDoNamespaces(true);
    domParser->setDoSchema(true);
}

Fibex::~Fibex()
{
    /* clean up */
    delete domParser;
    xercesc::XMLPlatformUtils::Terminate();
}

void Fibex::load(std::string & filename)
{
    load(filename.c_str());
}

void Fibex::load(const char * filename)
{
    /* load the document */
    try {
        domParser->parse(xercesc::XMLString::transcode(filename));

        /* get document */
        xercesc::DOMDocument * doc = domParser->getDocument();
        if (doc == nullptr)
            return;

        /* get root element */
        xercesc::DOMElement * docRootNode = doc->getDocumentElement();
        if (docRootNode == nullptr)
            return;

        /* load Fibex tree */
        fx::Fibex::load(docRootNode);
    } catch(...) {
        std::cerr << "Exception thrown." << std::endl;
        return;
    }
}

}
}
