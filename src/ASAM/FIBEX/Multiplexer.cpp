/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "Multiplexer.h"

namespace ASAM {
namespace FIBEX {

Multiplexer::Fx::Fx() :
    switch_(),
    dynamicPart(),
    staticPart()
{
}

Multiplexer::Multiplexer() :
    fx()
{
}

bool Multiplexer::load(xercesc::DOMElement * element)
{
    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());

        /* switch */
        if (nodeName == "fx:SWITCH") {
            fx.switch_.load(childElement);
        } else

        /* dynamicPart */
        if (nodeName == "fx:DYNAMIC-PART") {
            fx.dynamicPart.load(childElement);
        } else

        /* staticPart */
        if (nodeName == "fx:STATIC-PART") {
            fx.staticPart.load(childElement);
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }

    return true;
}

}
}
