<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:can="http://www.asam.net/xml/fbx/can" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fx="http://www.asam.net/xml/fbx" targetNamespace="http://www.asam.net/xml/fbx/can" elementFormDefault="qualified" attributeFormDefault="unqualified" version="2.1.0">
	<xs:import namespace="http://www.asam.net/xml/fbx" schemaLocation="fibex.xsd"/>
	<xs:complexType name="CONTROLLER-TYPE">
		<xs:annotation>
			<xs:documentation>The CAN-specific extension of the common CONTROLLER-TYPE</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="fx:CONTROLLER-TYPE">
				<xs:choice>
					<xs:element name="CAN-BIT-TIMING" type="can:CAN-BIT-TIMING-TYPE">
						<xs:annotation>
							<xs:documentation>Specification of a sending behaviour where the transmission order is predefined, e.g. used on LIN clusters</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="CAN-BIT-TIMING-REQUIREMENTS" type="can:CAN-BIT-TIMING-REQUIREMENT-TYPE">
						<xs:annotation>
							<xs:documentation>Specification of a sending behaviour where the exact time for the frames transmission is guaranteed, e.g. used on FLEXRAY clusters</xs:documentation>
						</xs:annotation>
					</xs:element>
				</xs:choice>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="CAN-BIT-TIMING-TYPE" abstract="false">
		<xs:annotation>
			<xs:documentation>This element is used for the specification of the exact CAN Bit Timing configuration parameter values. </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="TIME-SEG0" type="xs:positiveInteger" default="1">
				<xs:annotation>
					<xs:documentation>Time segment 0 in time quantums (TQ).</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="TIME-SEG1" type="xs:positiveInteger" default="2">
				<xs:annotation>
					<xs:documentation>Time segment 1 in time quantums (TQ).</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="SYNC-JUMP-WIDTH" type="xs:positiveInteger" default="58">
				<xs:annotation>
					<xs:documentation>Synchronization jump witdh in time quantums (TQ).</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="NUMBER-OF-SAMPLES" type="xs:positiveInteger" default="1" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Number of samples. Possible values are 1 or 3.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="CAN-BIT-TIMING-REQUIREMENT-TYPE" abstract="false">
		<xs:annotation>
			<xs:documentation>This element allows the specification of ranges for the CAN Bit Timing configuration parameters. These ranges are taken as requirements and have to be respected by the ECU developer.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="MAX-NUMER-OF-TIME-QUANTA-PER-BIT" type="xs:positiveInteger" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Maximum number of time quanta in the bit time.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="MAX-SAMPLE-POINT" type="xs:float" minOccurs="0">
				<xs:annotation>
					<xs:documentation>The max. value of the sample point as a percentage of the total bit time. </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="MAX-SYNC-JUMP-WIDTH" type="xs:float" minOccurs="0">
				<xs:annotation>
					<xs:documentation>The max. Synchronization Jump Width value as a percentage of the total bit time. The (Re-)Synchronization Jump Width (SJW) defines how far a resynchronization may move the Sample Point inside the limits defined by the Phase Buffer Segments to compensate for edge phase errors.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="MIN-NUMBER-OF-TIME-QUANTA-PER-BIT" type="xs:positiveInteger" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Minimum number of time quanta in the bit time.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="MIN-SAMPLE-POINT" type="xs:float" minOccurs="0">
				<xs:annotation>
					<xs:documentation>The min. value of the sample point as a percentage of the total bit time. </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="MIN-SYNC-JUMP-WIDTH" type="xs:float" minOccurs="0">
				<xs:annotation>
					<xs:documentation>The min. Synchronization Jump Width value as a percentage of the total bit time. The (Re-)Synchronization Jump Width (SJW) defines how far a resynchronization may move the Sample Point inside the limits defined by the Phase Buffer Segments to compensate for edge phase errors.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="IDENTIFIER-VALUE-TYPE">
		<xs:annotation>
			<xs:documentation>The CAN-specific extension of the common IDENTIFIER-VALUE-TYPE</xs:documentation>
		</xs:annotation>
		<xs:simpleContent>
			<xs:extension base="fx:IDENTIFIER-VALUE-TYPE">
				<xs:attribute name="EXTENDED-ADDRESSING" type="xs:boolean" use="optional" fixed="true">
					<xs:annotation>
						<xs:documentation>A flag indicating whether extended addresses are used.</xs:documentation>
					</xs:annotation>
				</xs:attribute>
			</xs:extension>
		</xs:simpleContent>
	</xs:complexType>
	<xs:simpleType name="PROTOCOL-TYPE">
		<xs:annotation>
			<xs:documentation>The CAN-specific restriction of the common PROTOCOL-TYPE</xs:documentation>
		</xs:annotation>
		<xs:restriction base="fx:PROTOCOL-TYPE">
			<xs:enumeration value="CAN"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="PHYSICAL-TYPE">
		<xs:annotation>
			<xs:documentation>The CAN-specific restriction of the common PHYSICAL-TYPE. ISO_11898-2 is common High-Speed-CAN and ISO_11898-3 is common Low-Speed-CAN.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="fx:PHYSICAL-TYPE">
			<xs:enumeration value="ISO_11898-2"/>
			<xs:enumeration value="ISO_11898-3"/>
		</xs:restriction>
	</xs:simpleType>
</xs:schema>
