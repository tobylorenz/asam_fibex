<?xml version="1.0" encoding="UTF-8"?>
<xs:schema targetNamespace="http://www.asam.net/xml/fbx/tp/frtp/asr30" xmlns:asr30tp="http://www.asam.net/xml/fbx/tp/asr30" xmlns:asr30frtp="http://www.asam.net/xml/fbx/tp/frtp/asr30" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:ho="http://www.asam.net/xml" xmlns:flexray="http://www.asam.net/xml/fbx/flexray" xmlns:fx="http://www.asam.net/xml/fbx" elementFormDefault="qualified" attributeFormDefault="unqualified" version="1.0.0">
	<xs:import namespace="http://www.asam.net/xml/fbx" schemaLocation="fibex.xsd"/>
	<xs:import namespace="http://www.asam.net/xml/fbx/flexray" schemaLocation="fibex4flexray.xsd"/>
	<xs:import namespace="http://www.asam.net/xml/fbx/tp/asr30" schemaLocation="fibex4tpASR30.xsd"/>
	<xs:complexType name="TP-NODE-TYPE">
		<xs:annotation>
			<xs:documentation>Contains the FlexRay TP-node specific elements for ASR TP 3.0</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="asr30tp:TP-NODE-TYPE">
				<xs:sequence>
					<xs:element name="BUFFER-REQUEST" type="xs:unsignedByte">
						<xs:annotation>
							<xs:documentation>AUTOSAR-name: FRTP_MAX_BUFREQ; This parameter defines the maximum number of trying to get a buffer (Transmit / Receive)</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="MAX-AR" type="xs:unsignedByte">
						<xs:annotation>
							<xs:documentation>AUTOSAR-name: FRTP_MAX_AR;</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="MAX-AS" type="xs:unsignedByte">
						<xs:annotation>
							<xs:documentation>AUTOSAR-name: This parameter defines the maximum number of trying to send a frame when a TIMEOUT AR occurs.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="MAX-FRIF" type="xs:unsignedByte">
						<xs:annotation>
							<xs:documentation>AUTOSAR-name: FRTP_MAX_FRIF; This parameter defines the maximum number of trying to send a frame when the FrIf returns an error</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="TIME-BUFFER" type="xs:duration">
						<xs:annotation>
							<xs:documentation>AUTOSAR-name: FRTP_TIME_BUFFER; This parameter defines the time of waiting for the next try to get a Tx or Rx buffer.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="TIME-FRIF" type="xs:duration">
						<xs:annotation>
							<xs:documentation>AUTOSAR-name: FRTP_TIME_FRIF; This parameter defines the time of waiting for the next try to send</xs:documentation>
						</xs:annotation>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="TP-CHANNEL-TYPE">
		<xs:annotation>
			<xs:documentation>Contains the FlexRay TP-cannel specific elements for ASR TP 3.0</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="asr30tp:TP-CHANNEL-TYPE">
				<xs:sequence>
					<xs:element name="ACKTYPE" type="asr30frtp:ACKTYPE"/>
					<xs:element name="ADDRESSING-TYPE" type="asr30frtp:ADDRESSING-TYPE"/>
					<xs:element name="GROUP-SEGMENTATION" type="xs:boolean">
						<xs:annotation>
							<xs:documentation>AUTOSAR-name: FRTP_GRPSEG; Here can be specified, whether segmentation within a 1:n connection is allowed or not.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="MAX-BLOCK-SIZE">
						<xs:annotation>
							<xs:documentation>AUTOSAR-name: FRTP_MAXBS; This parameter is only relevant when having retry activated. It limits the maximal block size the FrTp can choose in order to limit the amount of Tx buffer that will be requested at the sender side in a segmented transfer</xs:documentation>
						</xs:annotation>
						<xs:simpleType>
							<xs:restriction base="xs:unsignedByte">
								<xs:minInclusive value="1"/>
								<xs:maxInclusive value="16"/>
							</xs:restriction>
						</xs:simpleType>
					</xs:element>
					<xs:element name="MAX-RETRIES" type="xs:unsignedByte">
						<xs:annotation>
							<xs:documentation>AUTOSAR-name: FRTP_MAX_RN; This parameter defines the maximum number of retries (if retry is configured for the particular channel).</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="MAXIMUM-MESSAGE-LENGTH" type="asr30frtp:MAXIMUM-MESSAGE-LENGTH"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:simpleType name="ACKTYPE">
		<xs:annotation>
			<xs:documentation>AUTOSAR-name: FRTP_ACKTYPE; This parameter defines the type of acknowledgement which is used for the specific channel</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:token">
			<xs:enumeration value="FRTP_NO">
				<xs:annotation>
					<xs:documentation>no acknowledgement</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
			<xs:enumeration value="FRTP_ACK_WITHOUT_RT">
				<xs:annotation>
					<xs:documentation>acknowledgement but no retry</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
			<xs:enumeration value="FRTP_ACK_WITH_RT">
				<xs:annotation>
					<xs:documentation>acknowledgement with retry</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="ADDRESSING-TYPE">
		<xs:annotation>
			<xs:documentation>AUTOSAR-name: FRTP_ADRTYPE; This parameter states the addressing type this connection has. The meanings of the values are one byte and two byte.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:token">
			<xs:enumeration value="FRTP_OB">
				<xs:annotation>
					<xs:documentation>One Byte</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
			<xs:enumeration value="FRTP_TB">
				<xs:annotation>
					<xs:documentation>Two Bytes</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="MAXIMUM-MESSAGE-LENGTH">
		<xs:annotation>
			<xs:documentation>AUTOSAR-name: FRTP_LM; This specifies the maximum message length for the particular channel.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:token">
			<xs:enumeration value="FRTP_ISO">
				<xs:annotation>
					<xs:documentation>Up to (2^12)-1 Byte message length (No FF-E or SF-E shall be used and recognized)</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
			<xs:enumeration value="FRTP_ISO6">
				<xs:annotation>
					<xs:documentation>As ISO, but the maximum payload length is limited to 6 byte (SF-I, FF-I, CF). This is necessary to route TP on CAN when using Extended Addressing or Mixed Addressing on CAN.</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
			<xs:enumeration value="FRTP_L4G">
				<xs:annotation>
					<xs:documentation>SF-E allowed (SF of arbitrary length depending on FRTP_PDU_LENGTH), up to (2^32)-1 byte message length (all FF-x allowed) allowed)</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
		</xs:restriction>
	</xs:simpleType>
	<xs:complexType name="CONNECTOR-REFS-TYPE">
		<xs:complexContent>
			<xs:restriction base="asr30tp:CONNECTOR-REFS-TYPE">
				<xs:sequence>
					<xs:element ref="fx:CONNECTOR-REF" maxOccurs="2"/>
				</xs:sequence>
			</xs:restriction>
		</xs:complexContent>
	</xs:complexType>
</xs:schema>
