<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:lin="http://www.asam.net/xml/fbx/lin" xmlns:ho="http://www.asam.net/xml" xmlns:fx="http://www.asam.net/xml/fbx" xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="http://www.asam.net/xml/fbx/lin" elementFormDefault="qualified" attributeFormDefault="unqualified" version="2.0.2">
	<xs:import namespace="http://www.asam.net/xml/fbx" schemaLocation="fibex.xsd"/>
	<xs:import namespace="http://www.asam.net/xml" schemaLocation="harmonizedObjects.xsd"/>
	<xs:complexType name="APPLICATION-SCHEDULE-TABLE-ENTRY-TYPE">
		<xs:annotation>
			<xs:documentation>Schedule table entry for application messages.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="lin:SCHEDULE-TABLE-ENTRY-TYPE">
				<xs:sequence>
					<xs:element ref="fx:FRAME-TRIGGERING-REF"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="ASSIGN-FRAME-PID-RANGE-ENTRY-TYPE">
		<xs:annotation>
			<xs:documentation>Assign frame ID range is used to set or disable PIDs up to four frames.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="lin:CONFIGURATION-SCHEDULE-TABLE-ENTRY-TYPE">
				<xs:sequence>
					<xs:element name="START-INDEX" type="xs:unsignedByte">
						<xs:annotation>
							<xs:documentation>This specifies which is the first frame to assign a PID.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="ORDERED-PIDS">
						<xs:annotation>
							<xs:documentation>Array of four PID values that will be used in the configuration request.</xs:documentation>
						</xs:annotation>
						<xs:complexType>
							<xs:sequence>
								<xs:element name="ORDERED-PID" type="lin:ORDERED-PID-TYPE" minOccurs="4" maxOccurs="4"/>
							</xs:sequence>
						</xs:complexType>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="ASSIGN-FRAME-TRIGGERING-ENTRY-TYPE">
		<xs:annotation>
			<xs:documentation>This is a LIN2.0 feature which has been removed with LIN2.1. It is used to set a valid protected identifier to a frame specific by its message identifier.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="lin:CONFIGURATION-SCHEDULE-TABLE-ENTRY-TYPE">
				<xs:sequence>
					<xs:element name="ASSIGNED-FRAME-TRIGGERING">
						<xs:annotation>
							<xs:documentation>Refering the frameTriggering to assign.</xs:documentation>
						</xs:annotation>
						<xs:complexType>
							<xs:sequence>
								<xs:element ref="fx:FRAME-TRIGGERING-REF"/>
								<xs:element ref="lin:MESSAGE-ID-REF"/>
							</xs:sequence>
						</xs:complexType>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="ASSIGN-NAD-ENTRY-TYPE">
		<xs:annotation>
			<xs:documentation>Entry type for assign NAD services.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="lin:CONFIGURATION-SCHEDULE-TABLE-ENTRY-TYPE">
				<xs:sequence>
					<xs:element name="NEW-NAD" type="xs:unsignedByte">
						<xs:annotation>
							<xs:documentation>The NAD to assign.</xs:documentation>
						</xs:annotation>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="CAPABILITY">
		<xs:annotation>
			<xs:documentation>Describing the capability of a LIN slave. This section refers to the peculiarities of the LIN capability file.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="SENDS-WAKE-UP-SIGNAL" type="xs:boolean">
				<xs:annotation>
					<xs:documentation>This parameter  ist set to yes if the slaves is able to transmit the wake up signal; otherwise no.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="SPEED" type="lin:SPEED"/>
			<xs:element name="INITIAL-NAD" type="lin:INITIAL-NAD"/>
			<xs:element name="DIAGNOSTIC-CLASS">
				<xs:annotation>
					<xs:documentation>Defining the supported diagnostic class 1, 2 or 3.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:unsignedByte">
						<xs:enumeration value="1"/>
						<xs:enumeration value="2"/>
						<xs:enumeration value="3"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="N-AS-TIMEOUT" type="xs:duration" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Time for transmission of the LIN frame on the transmitter side.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="N-CR-TIMEOUT" type="xs:duration" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Time until reception of the next consecutive frame.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="SUPPORTED-SERVICE-IDS" minOccurs="0">
				<xs:annotation>
					<xs:documentation>All diagnostic services that aresupported by the refering LIN slave.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence>
						<xs:element name="SUPPORTED-SERVICE-ID" type="xs:unsignedByte" maxOccurs="unbounded">
							<xs:annotation>
								<xs:documentation>A diagnostic service given by its ID that is supported by the refering LIN slave.</xs:documentation>
							</xs:annotation>
						</xs:element>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="MAX-DIAG-FRAME-BYTE-LENGTH" type="xs:unsignedShort" minOccurs="0">
				<xs:annotation>
					<xs:documentation>The maximum length of a segmented frame in bytes, usually longer than 8 byte.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="VOLTAGE-RANGE" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Giving the supply voltage range within which the node is working.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence>
						<xs:element ref="ho:LOWER-LIMIT"/>
						<xs:element ref="ho:UPPER-LIMIT"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="TEMPERATURE-RANGE" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Giving the temperature range within which the node is working.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence>
						<xs:element ref="ho:LOWER-LIMIT"/>
						<xs:element ref="ho:UPPER-LIMIT"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="CONFORMANCE" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Specifying the conformance test that has been applied to the interface of the node. Usually “LIN2.0” or “NONE”.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:token">
						<xs:enumeration value="LIN2.0"/>
						<xs:enumeration value="NONE"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="CHANNEL-TYPE">
		<xs:annotation>
			<xs:documentation>LIN specific extension of CHANNEL-TYPE.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="fx:CHANNEL-TYPE">
				<xs:sequence>
					<xs:element name="SCHEDULE-TABLES">
						<xs:annotation>
							<xs:documentation>Top-level element for all schedule tables of a channel.</xs:documentation>
						</xs:annotation>
						<xs:complexType>
							<xs:sequence>
								<xs:element name="SCHEDULE-TABLE" type="lin:SCHEDULE-TABLE-TYPE" maxOccurs="unbounded">
									<xs:annotation>
										<xs:documentation>Send behavior of a lin master</xs:documentation>
									</xs:annotation>
								</xs:element>
							</xs:sequence>
						</xs:complexType>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="CONDITIONAL-CHANGE-NAD-ENTRY-TYPE">
		<xs:annotation>
			<xs:documentation>Entry type for conditional change NAD which is used to detect unknown slave nodes in a cluster and to separate their NAD.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="lin:CONFIGURATION-SCHEDULE-TABLE-ENTRY-TYPE">
				<xs:sequence>
					<xs:element name="BYTE" type="xs:unsignedByte">
						<xs:annotation>
							<xs:documentation>Byte to become extracted by slave node.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="ID" type="xs:unsignedByte">
						<xs:annotation>
							<xs:documentation>Identifier to select response.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="INVERT" type="xs:unsignedByte">
						<xs:annotation>
							<xs:documentation>Value for bitwise XOR done by slave node.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="MASK" type="xs:unsignedByte">
						<xs:annotation>
							<xs:documentation>Value for bitwise AND done by slave node.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="NEW-NAD" type="xs:unsignedByte">
						<xs:annotation>
							<xs:documentation>The new NAD.</xs:documentation>
						</xs:annotation>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="CONFIGURABLE-FRAME-TYPE">
		<xs:sequence>
			<xs:element ref="lin:MESSAGE-ID"/>
			<xs:choice>
				<xs:element ref="lin:UNCONDITIONAL-FRAME-REF"/>
				<xs:element ref="lin:EVENT-TRIGGERED-FRAME-REF"/>
				<xs:element ref="lin:SPORADIC-FRAME-REF"/>
			</xs:choice>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="CONFIGURATION-SCHEDULE-TABLE-ENTRY-TYPE" abstract="true">
		<xs:annotation>
			<xs:documentation>A specific extension of the common SCHEDULE-TABLE-ENTRY-TYPE for configuration entries.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="lin:SCHEDULE-TABLE-ENTRY-TYPE">
				<xs:sequence>
					<xs:element name="ASSIGNED-SLAVE">
						<xs:annotation>
							<xs:documentation>The LIN slaves controller who is target of this assignment. </xs:documentation>
						</xs:annotation>
						<xs:complexType>
							<xs:sequence>
								<xs:element ref="lin:SLAVE-REF"/>
							</xs:sequence>
						</xs:complexType>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="CONNECTOR-TYPE">
		<xs:annotation>
			<xs:documentation>The LIN-specific extension of the common CONNECTOR-TYPE.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="fx:CONNECTOR-TYPE">
				<xs:sequence>
					<xs:element name="NAD" type="lin:NAD-TYPE" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Initial NAD of the LIN slave.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="ORDERED-CONFIGURABLE-FRAMES" minOccurs="0">
						<xs:annotation>
							<xs:documentation>This list is necessary for the LIN 2.1 Assign-Frame-PID-Range command. Please note that the order is important!</xs:documentation>
						</xs:annotation>
						<xs:complexType>
							<xs:sequence>
								<xs:element name="ORDERED-CONFIGURABLE-FRAME" type="lin:ORDERED-CONFIGURABLE-FRAME-TYPE" maxOccurs="unbounded"/>
							</xs:sequence>
						</xs:complexType>
					</xs:element>
					<xs:element name="CONFIGURABLE-FRAMES" minOccurs="0">
						<xs:annotation>
							<xs:documentation>This list is necessary for the LIN 2.0 Assign-Frame-Identifier command.</xs:documentation>
						</xs:annotation>
						<xs:complexType>
							<xs:sequence>
								<xs:element name="CONFIGURABLE-FRAME" type="lin:CONFIGURABLE-FRAME-TYPE" maxOccurs="unbounded"/>
							</xs:sequence>
						</xs:complexType>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="DATA-DUMP-ENTRY-TYPE">
		<xs:annotation>
			<xs:documentation>Entry type for initial configuration of a slave node. The format of the message is supplier specific.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="lin:CONFIGURATION-SCHEDULE-TABLE-ENTRY-TYPE">
				<xs:sequence>
					<xs:element ref="fx:BYTE-VALUE" minOccurs="5" maxOccurs="5"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="DATA-SCHEDULE-TABLE-ENTRY-TYPE">
		<xs:annotation>
			<xs:documentation>The extension maps the LIN specific free data frames into FIBEX. The base type keeps scheduling for those free data frames applicable.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="lin:SCHEDULE-TABLE-ENTRY-TYPE">
				<xs:sequence>
					<xs:element name="FREE-FORMAT">
						<xs:annotation>
							<xs:documentation>Representing freely defined data.</xs:documentation>
						</xs:annotation>
						<xs:complexType>
							<xs:sequence>
								<xs:element ref="fx:BYTE-VALUE" maxOccurs="8"/>
							</xs:sequence>
						</xs:complexType>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="EVENT-TRIGGERED-FRAME-TYPE">
		<xs:annotation>
			<xs:documentation>A LIN specific extension of the common FRAME to enable the usual frame handling of a placeholder frame that is substituted at runtime. Event-triggered frame must not declare signal instances nor multiplexers.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="fx:FRAME-TYPE">
				<xs:sequence>
					<xs:element name="UNCONDITIONAL-FRAME-REFS">
						<xs:annotation>
							<xs:documentation>Collecting the unconditional frames that are substituted by the refering one.</xs:documentation>
						</xs:annotation>
						<xs:complexType>
							<xs:sequence>
								<xs:element ref="lin:UNCONDITIONAL-FRAME-REF" maxOccurs="unbounded"/>
							</xs:sequence>
						</xs:complexType>
					</xs:element>
					<xs:element name="SCHEDULE-TABLE-REF" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Refering the collision-resolving LIN schedule table.</xs:documentation>
						</xs:annotation>
						<xs:complexType>
							<xs:attribute name="ID-REF" type="xs:IDREF" use="required"/>
						</xs:complexType>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="INITIAL-NAD">
		<xs:annotation>
			<xs:documentation>Contains a range or list of possible values for the initial node address of the refering LIN slave.</xs:documentation>
		</xs:annotation>
		<xs:choice>
			<xs:element name="INITIAL-NAD-RANGE">
				<xs:annotation>
					<xs:documentation>Giving the range of possible values that can be used as initial node address of the refering LIN slave.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence>
						<xs:element name="LOWER-LIMIT" type="lin:NAD-TYPE"/>
						<xs:element name="UPPER-LIMIT" type="lin:NAD-TYPE"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="INITIAL-NAD-LIST">
				<xs:annotation>
					<xs:documentation>Giving a list of possible values that can be used as initial node address of the refering LIN slave.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence>
						<xs:element name="NAD" type="lin:NAD-TYPE" maxOccurs="unbounded"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:choice>
	</xs:complexType>
	<xs:complexType name="MASTER-CONTROLLER-TYPE">
		<xs:annotation>
			<xs:documentation>Describing the properties of the refering ecu as a LIN master.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="fx:CONTROLLER-TYPE">
				<xs:sequence>
					<xs:element name="TIME-BASE" type="xs:duration">
						<xs:annotation>
							<xs:documentation>The time base of the refering LIN-master. See http://www.w3.org/TR/2001/REC-xmlschema-2-20010502/#duration for syntax explanation.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="JITTER" type="xs:duration">
						<xs:annotation>
							<xs:documentation>The jitter of the refering LIN-master. See http://www.w3.org/TR/2001/REC-xmlschema-2-20010502/#duration for syntax explanation.</xs:documentation>
						</xs:annotation>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="ORDERED-PID-TYPE">
		<xs:sequence>
			<xs:element name="INDEX" type="lin:PID-INDEX-TYPE">
				<xs:annotation>
					<xs:documentation>The index of the PID in the range from 0 to 3. The INDEX of PIDs within one ASSIGN-FRAME-PID-RANGE-ENTRY shall be unique, gaps are allowed.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element ref="lin:VALUE"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="MESSAGE-ID-REF">
		<xs:annotation>
			<xs:documentation>Refering a message-id of a frame</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:attribute name="ID-REF" type="xs:IDREF" use="required"/>
		</xs:complexType>
	</xs:element>
	<xs:complexType name="ORDERED-CONFIGURABLE-FRAME-TYPE">
		<xs:sequence>
			<xs:element name="INDEX" type="lin:INDEX-TYPE">
				<xs:annotation>
					<xs:documentation>The index of the CONFIGURABLE-FRAME. The INDEX shall not be duplicate within one CONNECTOR.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:choice>
				<xs:element ref="lin:UNCONDITIONAL-FRAME-REF"/>
				<xs:element ref="lin:EVENT-TRIGGERED-FRAME-REF"/>
				<xs:element ref="lin:SPORADIC-FRAME-REF"/>
			</xs:choice>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="ORDERED-FRAME-TYPE">
		<xs:annotation>
			<xs:documentation>Element to descripe a unconditional frame with an assigned priority.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="PRIORITY" type="xs:unsignedByte"/>
			<xs:element ref="lin:UNCONDITIONAL-FRAME-REF" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="SAVE-CONFIGURATION-ENTRY-TYPE">
		<xs:annotation>
			<xs:documentation>Entry type to notify a slave node to store its configuration.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="lin:CONFIGURATION-SCHEDULE-TABLE-ENTRY-TYPE"/>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="SCHEDULE-TABLE-ENTRY-TYPE" abstract="true">
		<xs:annotation>
			<xs:documentation>Content of a schedule table entry.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="fx:IDENTIFIABLE-ELEMENT-TYPE">
				<xs:sequence>
					<xs:element name="POSITION-IN-TABLE">
						<xs:annotation>
							<xs:documentation>Position of a schedule table entry in the associated schedule table.</xs:documentation>
						</xs:annotation>
						<xs:simpleType>
							<xs:restriction base="xs:unsignedInt">
								<xs:minInclusive value="0"/>
								<xs:maxInclusive value="63"/>
							</xs:restriction>
						</xs:simpleType>
					</xs:element>
					<xs:element name="DELAY" type="xs:duration">
						<xs:annotation>
							<xs:documentation>Relative delay of the frame described by this timing to its predescessor in the schedule table.</xs:documentation>
						</xs:annotation>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="SCHEDULE-TABLE-TYPE" abstract="false">
		<xs:annotation>
			<xs:documentation>Content model for the entity SCHEDULE-TABLE.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="fx:NAMED-ELEMENT-TYPE">
				<xs:sequence>
					<xs:element name="SCHEDULE-TABLE-ENTRIES" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Top-level Element for all Schedule table entries of a Schedule table.</xs:documentation>
						</xs:annotation>
						<xs:complexType>
							<xs:sequence>
								<xs:element name="SCHEDULE-TABLE-ENTRY" type="lin:SCHEDULE-TABLE-ENTRY-TYPE" maxOccurs="unbounded"/>
							</xs:sequence>
						</xs:complexType>
					</xs:element>
					<xs:element name="RESUME-POSITION" type="xs:unsignedByte">
						<xs:annotation>
							<xs:documentation/>
						</xs:annotation>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="SLAVE-CONTROLLER-TYPE">
		<xs:annotation>
			<xs:documentation>Describing the properties of the refering ecu as a LIN slave.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="fx:CONTROLLER-TYPE">
				<xs:sequence>
					<xs:element ref="fx:PROTOCOL-VERSION"/>
					<xs:element name="SUPPLIER-ID" type="xs:unsignedInt">
						<xs:annotation>
							<xs:documentation>Supplier ID as assigned by the LIN Consortium to each supplier.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="FUNCTION-ID" type="xs:unsignedInt">
						<xs:annotation>
							<xs:documentation>Identifier of the function.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="VARIANT" type="xs:unsignedByte">
						<xs:annotation>
							<xs:documentation>Identifier of the variant.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="CONFIGURED-NAD" type="lin:NAD-TYPE" minOccurs="0">
						<xs:annotation>
							<xs:documentation>To distinguish LIN slaves that are used twice or more within the same cluster. Note: Find the initial NAD as LOCAL-ADDRESS in the CONNECTOR.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="P2-MIN" type="xs:duration" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Timing property for segmented frames. Minimum time between master request and slave response.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="ST-MIN" type="xs:duration" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Timing property for segmented frames. Minimum time between two slave response frames.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="CAPABILITY" type="lin:CAPABILITY" minOccurs="0"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="SPEED">
		<xs:annotation>
			<xs:documentation>A channels speed in bits per second</xs:documentation>
		</xs:annotation>
		<xs:choice>
			<xs:element name="SPEED-RANGE">
				<xs:annotation>
					<xs:documentation>The node has automatic baud rate detection and can adapt itself to a speed in this range.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:choice>
						<xs:sequence>
							<xs:element name="LOWER-LIMIT" type="fx:SPEED-TYPE"/>
							<xs:element name="UPPER-LIMIT" type="fx:SPEED-TYPE" minOccurs="0"/>
						</xs:sequence>
						<xs:element name="UPPER-LIMIT" type="fx:SPEED-TYPE"/>
					</xs:choice>
				</xs:complexType>
			</xs:element>
			<xs:element name="SPEED-VALUES">
				<xs:annotation>
					<xs:documentation>The node supports certain fixed baud rates given as a discrete list.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence>
						<xs:element name="SPEED-VALUE" type="fx:SPEED-TYPE" maxOccurs="unbounded">
							<xs:annotation>
								<xs:documentation>A certain fixed baud rates (in bits per second).</xs:documentation>
							</xs:annotation>
						</xs:element>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:choice>
	</xs:complexType>
	<xs:complexType name="SPORADIC-FRAME-TYPE">
		<xs:annotation>
			<xs:documentation>A LIN specific extension of the common FRAME to enable the usual frame handling of a placeholder frame that is substituted at runtime. Sporadic frame must not declare signal instances nor multiplexers.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="fx:FRAME-TYPE">
				<xs:sequence>
					<xs:element name="ORDERED-FRAMES">
						<xs:annotation>
							<xs:documentation/>
						</xs:annotation>
						<xs:complexType>
							<xs:sequence>
								<xs:element name="ORDERED-FRAME" type="lin:ORDERED-FRAME-TYPE" maxOccurs="unbounded"/>
							</xs:sequence>
						</xs:complexType>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="UNASSIGN-FRAME-TRIGGERING-ENTRY-TYPE">
		<xs:annotation>
			<xs:documentation>Entry type for request that are used to disable reception/transmission of a dynamically assigned frame identifier.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="lin:CONFIGURATION-SCHEDULE-TABLE-ENTRY-TYPE">
				<xs:sequence>
					<xs:element name="UNASSIGNED-FRAME-TRIGGERING">
						<xs:annotation>
							<xs:documentation>Refering a frameTriggering to unassign.</xs:documentation>
						</xs:annotation>
						<xs:complexType>
							<xs:sequence>
								<xs:element ref="fx:FRAME-TRIGGERING-REF"/>
							</xs:sequence>
						</xs:complexType>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="UNCONDITIONAL-FRAME-TYPE">
		<xs:annotation>
			<xs:documentation>A LIN specific extension of the common FRAME to mark LIN unconditional frames.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="fx:FRAME-TYPE"/>
		</xs:complexContent>
	</xs:complexType>
	<xs:element name="EVENT-TRIGGERED-FRAME-REF">
		<xs:annotation>
			<xs:documentation>Referring an event-triggered frame.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:attribute name="ID-REF" type="xs:IDREF" use="required"/>
		</xs:complexType>
	</xs:element>
	<xs:element name="MESSAGE-ID">
		<xs:annotation>
			<xs:documentation>Message-Id of a frame.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:simpleContent>
				<xs:extension base="xs:unsignedByte">
					<xs:attribute name="ID" type="xs:ID" use="required"/>
				</xs:extension>
			</xs:simpleContent>
		</xs:complexType>
	</xs:element>
	<xs:element name="SLAVE-REF">
		<xs:annotation>
			<xs:documentation>Refering a LIN-slave</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:attribute name="ID-REF" type="xs:IDREF" use="required"/>
		</xs:complexType>
	</xs:element>
	<xs:element name="SPORADIC-FRAME-REF">
		<xs:annotation>
			<xs:documentation>Referring an sporadic frame.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:attribute name="ID-REF" type="xs:IDREF" use="required"/>
		</xs:complexType>
	</xs:element>
	<xs:element name="UNCONDITIONAL-FRAME-REF">
		<xs:annotation>
			<xs:documentation>Referring an unconditional frame.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:attribute name="ID-REF" type="xs:IDREF" use="required"/>
		</xs:complexType>
	</xs:element>
	<xs:element name="VALUE">
		<xs:annotation>
			<xs:documentation>The value of the PID.</xs:documentation>
		</xs:annotation>
		<xs:simpleType>
			<xs:restriction base="xs:unsignedByte">
				<xs:minInclusive value="0"/>
				<xs:maxInclusive value="255"/>
			</xs:restriction>
		</xs:simpleType>
	</xs:element>
	<xs:simpleType name="COMPOSITION-LEVEL-TYPE">
		<xs:annotation>
			<xs:documentation>The LIN-specific restriction of the common COMPOSITION-LEVEL-TYPE</xs:documentation>
		</xs:annotation>
		<xs:restriction base="fx:COMPOSITION-LEVEL-TYPE">
			<xs:enumeration value="GENERAL-COMPOSITE"/>
			<xs:enumeration value="CONFIGURATION"/>
			<xs:enumeration value="COMPOSITE-NODE"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="PID-INDEX-TYPE">
		<xs:restriction base="lin:INDEX-TYPE">
			<xs:minInclusive value="0"/>
			<xs:maxInclusive value="3"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="INDEX-TYPE">
		<xs:restriction base="xs:unsignedByte"/>
	</xs:simpleType>
	<xs:simpleType name="NAD-TYPE">
		<xs:annotation>
			<xs:documentation>Initial NAD of the LIN slave.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:unsignedByte"/>
	</xs:simpleType>
	<xs:simpleType name="PROTOCOL-TYPE">
		<xs:annotation>
			<xs:documentation>The LIN-specific restriction of the common PROTOCOL-TYPE</xs:documentation>
		</xs:annotation>
		<xs:restriction base="fx:PROTOCOL-TYPE">
			<xs:enumeration value="LIN"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="TYPE-TYPE">
		<xs:annotation>
			<xs:documentation>The LIN-specific extension of the general purpose of a signal.</xs:documentation>
		</xs:annotation>
		<xs:union memberTypes="fx:TYPE-TYPE">
			<xs:simpleType>
				<xs:restriction base="xs:token">
					<xs:enumeration value="FAULT-STATE"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:union>
	</xs:simpleType>
</xs:schema>
