/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "CyclicTiming.h"

namespace ASAM {
namespace FIBEX {

CyclicTiming::Fx::RepeatingTimeRange::RepeatingTimeRange() :
    value()
{
}

CyclicTiming::Fx::StartingTimeRange::StartingTimeRange() :
    value()
{
}

CyclicTiming::Fx::StartCondition::SignalTrigger::SignalTrigger() :
    signalState(),
    signalInstanceRef()
{
}

CyclicTiming::Fx::StartCondition::StartCondition() :
    signalTrigger()
{
}

CyclicTiming::Fx::StopCondition::SignalTrigger::SignalTrigger() :
    signalState(),
    signalInstanceRef()
{
}

CyclicTiming::Fx::StopCondition::StopCondition() :
    signalTrigger()
{
}

CyclicTiming::Fx::Fx() :
    repeatingTimeRange(),
    startingTimeRange(),
    startCondition(),
    stopCondition(),
    finalRepetitions()
{
}

CyclicTiming::Ho::Ho() :
    desc()
{
}

CyclicTiming::CyclicTiming() :
    fx(),
    ho()
{
}

bool CyclicTiming::load(xercesc::DOMElement * element)
{
    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());
        std::string value = xercesc::XMLString::transcode(childElement->getTextContent());

        /* repeatingTimeRange */
        if (nodeName == "fx:REPEATING-TIME-RANGE") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string nodeName2 = xercesc::XMLString::transcode(childElement2->getNodeName());
                std::string value2 = xercesc::XMLString::transcode(childElement2->getTextContent());

                /** value */
                if (nodeName2 == "fx:VALUE") {
                    fx.repeatingTimeRange.value = value2;
                } else

                {
                    std::cerr << "Unrecognized nodeName " << nodeName2 << std::endl;
                }
            }
        } else

        /* startingTimeRange */
        if (nodeName == "fx:STARTING-TIME-RANGE") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string nodeName2 = xercesc::XMLString::transcode(childElement2->getNodeName());
                std::string value2 = xercesc::XMLString::transcode(childElement2->getTextContent());

                /** value */
                if (nodeName2 == "fx:VALUE") {
                    fx.startingTimeRange.value = value2;
                } else

                {
                    std::cerr << "Unrecognized nodeName " << nodeName2 << std::endl;
                }
            }
        } else

        /* startCondition */
        if (nodeName == "fx:START-CONDITION") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string nodeName2 = xercesc::XMLString::transcode(childElement2->getNodeName());

                /* signalTrigger */
                if (nodeName2 == "fx:SIGNAL-TRIGGER") {
                    for (xercesc::DOMElement * childElement3 = childElement2->getFirstElementChild(); childElement3 != nullptr; childElement3 = childElement3->getNextElementSibling()) {
                        std::string nodeName3 = xercesc::XMLString::transcode(childElement3->getNodeName());

                        /* signalState */
                        if (nodeName3 == "fx:SIGNAL-STATE") {
                            std::string value3 = xercesc::XMLString::transcode(childElement3->getTextContent());

                            if (value3 == "VALUE_CHANGED") {
                                fx.startCondition.signalTrigger.signalState = Fx::StartCondition::SignalTrigger::SignalState::ValueChanged;
                            } else

                            if (value3 == "VALUE_NOT_CHANGED") {
                                fx.startCondition.signalTrigger.signalState = Fx::StartCondition::SignalTrigger::SignalState::ValueNotChanged;
                            } else

                            if (value3 == "VALUE_DEFAULT") {
                                fx.startCondition.signalTrigger.signalState = Fx::StartCondition::SignalTrigger::SignalState::ValueDefault;
                            } else

                            if (value3 == "VALUE_NOT_DEFAULT") {
                                fx.startCondition.signalTrigger.signalState = Fx::StartCondition::SignalTrigger::SignalState::ValueNotDefault;
                            } else

                            if (value3 == "OTHER") {
                                fx.startCondition.signalTrigger.signalState = Fx::StartCondition::SignalTrigger::SignalState::Other;
                            } else

                            {
                                std::cerr << "Unrecognized nodeName " << nodeName3 << std::endl;
                            }

                        } else

                        /* signalInstanceRef */
                        if (nodeName3 == "fx:SIGNAL-INSTANCE-REF") {
                            std::string id3 = xercesc::XMLString::transcode(childElement3->getAttribute(xercesc::XMLString::transcode("ID-REF")));
                            fx.startCondition.signalTrigger.signalInstanceRef = id3;
                        } else

                        {
                            std::cerr << "Unrecognized nodeName " << nodeName3 << std::endl;
                        }
                    }
                } else

                {
                    std::cerr << "Unrecognized nodeName " << nodeName2 << std::endl;
                }
            }
        } else

        /* stopCondition */
        if (nodeName == "fx:STOP-CONDITION") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string nodeName2 = xercesc::XMLString::transcode(childElement2->getNodeName());

                /* signalTrigger */
                if (nodeName2 == "fx:SIGNAL-TRIGGER") {
                    for (xercesc::DOMElement * childElement3 = childElement2->getFirstElementChild(); childElement3 != nullptr; childElement3 = childElement3->getNextElementSibling()) {
                        std::string nodeName3 = xercesc::XMLString::transcode(childElement3->getNodeName());

                        /* signalState */
                        if (nodeName3 == "fx:SIGNAL-STATE") {
                            std::string value3 = xercesc::XMLString::transcode(childElement3->getTextContent());

                            if (value3 == "VALUE_CHANGED") {
                                fx.stopCondition.signalTrigger.signalState = Fx::StopCondition::SignalTrigger::SignalState::ValueChanged;
                            } else

                            if (value3 == "VALUE_NOT_CHANGED") {
                                fx.stopCondition.signalTrigger.signalState = Fx::StopCondition::SignalTrigger::SignalState::ValueNotChanged;
                            } else

                            if (value3 == "VALUE_DEFAULT") {
                                fx.stopCondition.signalTrigger.signalState = Fx::StopCondition::SignalTrigger::SignalState::ValueDefault;
                            } else

                            if (value3 == "VALUE_NOT_DEFAULT") {
                                fx.stopCondition.signalTrigger.signalState = Fx::StopCondition::SignalTrigger::SignalState::ValueNotDefault;
                            } else

                            if (value3 == "OTHER") {
                                fx.stopCondition.signalTrigger.signalState = Fx::StopCondition::SignalTrigger::SignalState::Other;
                            } else

                            {
                                std::cerr << "Unrecognized nodeName " << nodeName3 << std::endl;
                            }

                        } else

                        /* signalInstanceRef */
                        if (nodeName3 == "fx:SIGNAL-INSTANCE-REF") {
                            std::string id3 = xercesc::XMLString::transcode(childElement3->getAttribute(xercesc::XMLString::transcode("ID-REF")));
                            fx.stopCondition.signalTrigger.signalInstanceRef = id3;
                        } else

                        {
                            std::cerr << "Unrecognized nodeName " << nodeName3 << std::endl;
                        }
                    }
                } else

                {
                    std::cerr << "Unrecognized nodeName " << nodeName2 << std::endl;
                }
            }
        } else

        /* finalRepetitions */
        if (nodeName == "fx:FINAL-REPETITIONS") {
            fx.finalRepetitions = stoul(value);
        } else

        /* desc */
        if (nodeName == "ho:DESC") {
            ho.desc = value;
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }

    return true;
}

}
}
