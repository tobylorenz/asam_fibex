/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "platform.h"

#include <string>
#include <xercesc/dom/DOM.hpp>

#include "asam_fibex_export.h"

namespace ASAM {
namespace FIBEX {

/**
 * CODED-TYPE
 */
class ASAM_FIBEX_EXPORT CodedType
{
public:
    CodedType();

    /** load from XML DOM element */
    bool load(xercesc::DOMElement * element);

    /** CATEGORY */
    enum class Category {
        /** LEADING-LENGTH-INFO-TYPE */
        LeadingLengthInfoType,

        /** END-OF-PDU */
        EndOfPdu,

        /** MIN-MAX-LENGTH-TYPE */
        MinMaxLengthType,

        /** STANDARD-LENGTH-TYPE */
        StandardLengthType
    } category;

    /** ENCODING */
    enum class Encoding {
        /** SIGNED */
        Signed,

        /** UNSIGNED */
        Unsigned,

        /** BIT */
        Bit,

        /** IEEE-FLOATING-TYPE */
        IeeeFloatingType,

        /** BCD */
        Bcd,

        /** DSP-FRACTIONAL */
        DspFractional,

        /** SM */
        Sm,

        /** BCD-P */
        BcdP,

        /** BCD-UP */
        BcdUp,

        /** 1C */
        _1c,

        /** 2C */
        _2c,

        /** UTF-8 */
        Utf8,

        /** UCS-2 */
        Ucs2,

        /** ISO-8859-1 */
        Iso8859_1,

        /** ISO-8859-2 */
        Iso8859_2,

        /** WINDOWS-1252 */
        Windows1252
    } encoding;

    /** ho */
    struct Ho {
        Ho();

        /** BIT-LENGTH */
        unsigned int bitLength;
    } ho;
};

}
}
