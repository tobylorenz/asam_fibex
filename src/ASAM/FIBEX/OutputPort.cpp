/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "OutputPort.h"

namespace ASAM {
namespace FIBEX {

OutputPort::Fx::Fx() :
    frameTriggeringRef(),
    completeFrame(false)
{
}

OutputPort::OutputPort() :
    id(),
    fx()
{
}

bool OutputPort::load(xercesc::DOMElement * element)
{
    /* read attributes */
    id = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("ID")));

    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());

        /* frameTriggeringRef */
        if (nodeName == "fx:FRAME-TRIGGERING-REF") {
            std::string id2 = xercesc::XMLString::transcode(childElement->getAttribute(xercesc::XMLString::transcode("ID-REF")));
            fx.frameTriggeringRef = id2;
        } else

        /* completeFrame */
        if (nodeName == "fx:COMPLETE-FRAME") {
            fx.completeFrame = true;
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }

    return true;
}

}
}
