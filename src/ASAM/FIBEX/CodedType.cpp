/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "CodedType.h"

namespace ASAM {
namespace FIBEX {

CodedType::Ho::Ho() :
    bitLength()
{
}

CodedType::CodedType() :
    category(),
    encoding(),
    ho()
{
}

bool CodedType::load(xercesc::DOMElement * element)
{
    /* read attributes */
    std::string categoryStr = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("CATEGORY")));
    if (categoryStr == "LEADING-LENGTH-INFO-TYPE") {
        category = Category::LeadingLengthInfoType;
    } else
    if (categoryStr == "END-OF-PDU") {
        category = Category::EndOfPdu;
    } else
    if (categoryStr == "MIN-MAX-LENGTH-TYPE") {
        category = Category::MinMaxLengthType;
    } else
    if (categoryStr == "STANDARD-LENGTH-TYPE") {
        category = Category::StandardLengthType;
    } else
    {
        std::string nodeName = xercesc::XMLString::transcode(element->getNodeName());
        std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
    }
    std::string encodingStr = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("ENCODING")));
    if (encodingStr == "SIGNED") {
        encoding = Encoding::Signed;
    } else
    if (encodingStr == "UNSIGNED") {
        encoding = Encoding::Unsigned;
    } else
    if (encodingStr == "BIT") {
        encoding = Encoding::Bit;
    } else
    if (encodingStr == "IEEE-FLOATING-TYPE") {
        encoding = Encoding::IeeeFloatingType;
    } else
    if (encodingStr == "BCD") {
        encoding = Encoding::Bcd;
    } else
    if (encodingStr == "DSP-FRACTIONAL") {
        encoding = Encoding::DspFractional;
    } else
    if (encodingStr == "SM") {
        encoding = Encoding::Sm;
    } else
    if (encodingStr == "BCD-P") {
        encoding = Encoding::BcdP;
    } else
    if (encodingStr == "BCD-UP") {
        encoding = Encoding::BcdUp;
    } else
    if (encodingStr == "1C") {
        encoding = Encoding::_1c;
    } else
    if (encodingStr == "2C") {
        encoding = Encoding::_2c;
    } else
    if (encodingStr == "UTF-8") {
        encoding = Encoding::Utf8;
    } else
    if (encodingStr == "UCS-2") {
        encoding = Encoding::Ucs2;
    } else
    if (encodingStr == "ISO-8859-1") {
        encoding = Encoding::Iso8859_1;
    } else
    if (encodingStr == "ISO-8859-2") {
        encoding = Encoding::Iso8859_2;
    } else
    if (encodingStr == "WINDOWS-1252") {
        encoding = Encoding::Windows1252;
    } else
    {
        std::string nodeName = xercesc::XMLString::transcode(element->getNodeName());
        std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
    }

    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());
        std::string value = xercesc::XMLString::transcode(childElement->getTextContent());

        /* shortName */
        if (nodeName == "ho:BIT-LENGTH") {
            ho.bitLength = stoul(value);
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }

    return true;
}

}
}
