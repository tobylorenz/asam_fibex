/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "EventControlledTiming.h"

namespace ASAM {
namespace FIBEX {

EventControlledTiming::Fx::DebounceTimeRange::Tolerance::Tolerance() :
    minValue(),
    maxValue()
{
}

EventControlledTiming::Fx::DebounceTimeRange::DebounceTimeRange() :
    value(),
    tolerance()
{
}

EventControlledTiming::Fx::SendCondition::SignalTrigger::SignalTrigger() :
    signalState(),
    signalInstanceRef()
{
}

EventControlledTiming::Fx::SendCondition::SendCondition() :
    signalTrigger()
{
}

EventControlledTiming::Fx::Fx() :
    debounceTimeRange(),
    sendCondition(),
    finalRepetions()
{
}

EventControlledTiming::EventControlledTiming() :
    fx()
{
}

bool EventControlledTiming::load(xercesc::DOMElement * element)
{
    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());
        std::string value = xercesc::XMLString::transcode(childElement->getTextContent());

        /* debounceTimeRange */
        if (nodeName == "fx:DEBOUNCE-TIME-RANGE") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string nodeName2 = xercesc::XMLString::transcode(childElement2->getNodeName());
                std::string value2 = xercesc::XMLString::transcode(childElement2->getTextContent());

                /* value */
                if (nodeName2 == "fx:VALUE") {
                    fx.debounceTimeRange.value = value2;
                } else

                /* value */
                if (nodeName2 == "fx:TOLERANCE") {
                    for (xercesc::DOMElement * childElement3 = childElement2->getFirstElementChild(); childElement3 != nullptr; childElement3 = childElement3->getNextElementSibling()) {
                        std::string nodeName3 = xercesc::XMLString::transcode(childElement3->getNodeName());
                        std::string value3 = xercesc::XMLString::transcode(childElement3->getTextContent());

                        /* minValue */
                        if (nodeName3 == "fx:MIN-VALUE") {
                            fx.debounceTimeRange.tolerance.minValue = value3;
                        } else

                        /* maxValue */
                        if (nodeName3 == "fx:MAX-VALUE") {
                            fx.debounceTimeRange.tolerance.maxValue = value3;
                        } else

                        {
                            std::cerr << "Unrecognized nodeName " << nodeName3 << std::endl;
                        }
                    }
                } else

                {
                    std::cerr << "Unrecognized nodeName " << nodeName2 << std::endl;
                }
            }
        } else

        /* sendCondition */
        if (nodeName == "fx:SEND-CONDITION") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string nodeName2 = xercesc::XMLString::transcode(childElement2->getNodeName());

                /* signalTrigger */
                if (nodeName2 == "fx:SIGNAL-TRIGGER") {
                    for (xercesc::DOMElement * childElement3 = childElement2->getFirstElementChild(); childElement3 != nullptr; childElement3 = childElement3->getNextElementSibling()) {
                        std::string nodeName3 = xercesc::XMLString::transcode(childElement3->getNodeName());

                        /* signalState */
                        if (nodeName3 == "fx:SIGNAL-STATE") {
                            std::string value3 = xercesc::XMLString::transcode(childElement3->getTextContent());

                            if (value3 == "VALUE_CHANGED") {
                                fx.sendCondition.signalTrigger.signalState = Fx::SendCondition::SignalTrigger::SignalState::ValueChanged;
                            } else

                            if (value3 == "VALUE_NOT_CHANGED") {
                                fx.sendCondition.signalTrigger.signalState = Fx::SendCondition::SignalTrigger::SignalState::ValueNotChanged;
                            } else

                            if (value3 == "VALUE_DEFAULT") {
                                fx.sendCondition.signalTrigger.signalState = Fx::SendCondition::SignalTrigger::SignalState::ValueDefault;
                            } else

                            if (value3 == "VALUE_NOT_DEFAULT") {
                                fx.sendCondition.signalTrigger.signalState = Fx::SendCondition::SignalTrigger::SignalState::ValueNotDefault;
                            } else

                            if (value3 == "OTHER") {
                                fx.sendCondition.signalTrigger.signalState = Fx::SendCondition::SignalTrigger::SignalState::Other;
                            } else

                            {
                                std::cerr << "Unrecognized nodeName " << nodeName3 << std::endl;
                            }

                        } else

                        /* signalInstanceRef */
                        if (nodeName3 == "fx:SIGNAL-INSTANCE-REF") {
                            std::string id3 = xercesc::XMLString::transcode(childElement3->getAttribute(xercesc::XMLString::transcode("ID-REF")));
                            fx.sendCondition.signalTrigger.signalInstanceRef = id3;
                        } else

                        {
                            std::cerr << "Unrecognized nodeName " << nodeName3 << std::endl;
                        }
                    }
                } else

                {
                    std::cerr << "Unrecognized nodeName " << nodeName2 << std::endl;
                }
            }
        } else

        /* finalRepetitions */
        if (nodeName == "fx:FINAL-REPETITIONS") {
            fx.finalRepetions = stoul(value);
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }

    return true;
}

}
}
