/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "InternalConstrs.h"

namespace ASAM {
namespace FIBEX {

InternalConstrs::ScaleConstr::LowerLimit::LowerLimit() :
    intervalType(IntervalType::Closed),
    value()
{
}

InternalConstrs::ScaleConstr::UpperLimit::UpperLimit() :
    intervalType(IntervalType::Closed),
    value()
{
}

InternalConstrs::ScaleConstr::ScaleConstr() :
    lowerLimit(),
    upperLimit()
{
}

InternalConstrs::InternalConstrs() :
    scaleConstr()
{
}

bool InternalConstrs::load(xercesc::DOMElement * element)
{
    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());

        /* scaleConstr */
        if (nodeName == "ho:SCALE-CONSTR") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string nodeName2 = xercesc::XMLString::transcode(childElement2->getNodeName());

                /* lowerLimit */
                if (nodeName2 == "ho:LOWER-LIMIT") {
                    /* intervalType */
                    std::string intervalType = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("INTERVAL-TYPE")));
                    if (!intervalType.empty()) {
                        if (intervalType == "OPEN") {
                            scaleConstr.lowerLimit.intervalType = ScaleConstr::LowerLimit::IntervalType::Open;
                        } else
                        if (intervalType == "CLOSED") {
                            scaleConstr.lowerLimit.intervalType = ScaleConstr::LowerLimit::IntervalType::Closed;
                        } else
                        if (intervalType == "INFINITE") {
                            scaleConstr.lowerLimit.intervalType = ScaleConstr::LowerLimit::IntervalType::Infinite;
                        } else
                        {
                            std::cerr << "Unrecognized nodeName " << nodeName2 << std::endl;
                        }
                    }

                    /* value */
                    std::string value = xercesc::XMLString::transcode(childElement->getTextContent());
                    scaleConstr.lowerLimit.value = stod(value);
                } else

                /* upperLimit */
                if (nodeName2 == "ho:UPPER-LIMIT") {
                    /* intervalType */
                    std::string intervalType = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("INTERVAL-TYPE")));
                    if (!intervalType.empty()) {
                        if (intervalType == "OPEN") {
                            scaleConstr.upperLimit.intervalType = ScaleConstr::UpperLimit::IntervalType::Open;
                        } else
                        if (intervalType == "CLOSED") {
                            scaleConstr.upperLimit.intervalType = ScaleConstr::UpperLimit::IntervalType::Closed;
                        } else
                        if (intervalType == "INFINITE") {
                            scaleConstr.upperLimit.intervalType = ScaleConstr::UpperLimit::IntervalType::Infinite;
                        } else
                        {
                            std::cerr << "Unrecognized nodeName " << nodeName2 << std::endl;
                        }
                    }

                    /* value */
                    std::string value = xercesc::XMLString::transcode(childElement->getTextContent());
                    scaleConstr.upperLimit.value = stod(value);
                } else
                {
                    std::cerr << "Unrecognized nodeName " << nodeName2 << std::endl;
                }
            }
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }

    return true;
}

}
}
