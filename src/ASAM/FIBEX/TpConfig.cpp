/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#include "TpConfig.h"

namespace ASAM {
namespace FIBEX {

TpConfig::Fx::Fx() :
    tpAddresses()
{
}

TpConfig::Asr30Tp::Asr30Tp() :
    tpChannels(),
    tpNodes(),
    tpEcus()
{
}

TpConfig::TpConfig() :
    ho::NameDetails(),
    xsiType(),
    id(),
    fx(),
    asr30tp()
{
}

bool TpConfig::load(xercesc::DOMElement * element)
{
    ho::NameDetails::load(element);

    /* read attributes */
    xsiType = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("xsi:type")));
    id = xercesc::XMLString::transcode(element->getAttribute(xercesc::XMLString::transcode("ID")));

    /* read childs */
    for (xercesc::DOMElement * childElement = element->getFirstElementChild(); childElement != nullptr; childElement = childElement->getNextElementSibling()) {
        std::string nodeName = xercesc::XMLString::transcode(childElement->getNodeName());
        std::string value = xercesc::XMLString::transcode(childElement->getTextContent());

        /* tpAddresses */
        if (nodeName == "fx:TP-ADDRESSES") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string id2 = xercesc::XMLString::transcode(childElement2->getAttribute(xercesc::XMLString::transcode("ID")));
                std::string value2 = xercesc::XMLString::transcode(childElement2->getTextContent());
                fx.tpAddresses[id2] = stoul(value2);
            }
        } else

        /* tpChannels */
        if (nodeName == "asr30tp:TP-CHANNELS") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string id2 = xercesc::XMLString::transcode(childElement2->getAttribute(xercesc::XMLString::transcode("ID")));
                asr30tp.tpChannels[id2].load(childElement2);
            }
        } else

        /* tpNodes */
        if (nodeName == "asr30tp:TP-NODES") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string id2 = xercesc::XMLString::transcode(childElement2->getAttribute(xercesc::XMLString::transcode("ID")));
                asr30tp.tpNodes[id2].load(childElement2);
            }
        } else

        /* tpEcus */
        if (nodeName == "asr30tp:TP-ECUS") {
            for (xercesc::DOMElement * childElement2 = childElement->getFirstElementChild(); childElement2 != nullptr; childElement2 = childElement2->getNextElementSibling()) {
                std::string id2 = xercesc::XMLString::transcode(childElement2->getAttribute(xercesc::XMLString::transcode("ID")));
                asr30tp.tpEcus[id2].load(childElement2);
            }
        } else

        {
            std::cerr << "Unrecognized nodeName " << nodeName << std::endl;
        }
    }

    return true;
}

}
}
