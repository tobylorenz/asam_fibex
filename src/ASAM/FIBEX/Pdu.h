/*
 * Copyright (C) 2013 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "platform.h"

#include <map>
#include <string>
#include <xercesc/dom/DOM.hpp>

#include "ho/NameDetails.h"
#include "Multiplexer.h"
#include "SignalInstance.h"

#include "asam_fibex_export.h"

namespace ASAM {
namespace FIBEX {

/**
 * PDU
 */
class ASAM_FIBEX_EXPORT Pdu : public ho::NameDetails
{
public:
    Pdu();

    /** load from XML DOM element */
    bool load(xercesc::DOMElement * element);

    /** ID */
    std::string id;

    /** fx */
    struct Fx {
        Fx();

        /** BYTE-LENGTH */
        unsigned int byteLength;

        /** PDU-TYPE */
        std::string pduType;

        /** MULTIPLEXER */
        Multiplexer multiplexer;

        /** SIGNAL-INSTANCES */
        std::map<std::string, SignalInstance> signalInstances;
    } fx;
};

}
}
