include_directories(${PROJECT_SOURCE_DIR}/src)

if(OPTION_RUN_EXAMPLES)

  find_package(XercesC)
  include_directories(${XercesC_INCLUDE_DIR})
  link_directories(${XercesC_LIBRARY_DIRS})

  add_definitions(
    -DCMAKE_CURRENT_SOURCE_DIR="${CMAKE_CURRENT_SOURCE_DIR}"
    -DCMAKE_CURRENT_BINARY_DIR="${CMAKE_CURRENT_BINARY_DIR}")

  set(CMAKE_STATIC_LIBRARY_PREFIX "${CMAKE_CURRENT_BINARY_DIR}")
  set(CMAKE_SHARED_LIBRARY_PREFIX "${CMAKE_CURRENT_BINARY_DIR}")

  if(WIN32)
    add_custom_target(ASAM_FIBEX-Copy ALL
      COMMAND ${CMAKE_COMMAND} -E copy_if_different
      "${PROJECT_BINARY_DIR}/src/ASAM/FIBEX/${CMAKE_BUILD_TYPE}/ASAM_FIBEX.dll"
      "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}"
      COMMENT "Copy ${PROJECT_BINARY_DIR}/src/ASAM/FIBEX/${CMAKE_BUILD_TYPE}/ASAM_FIBEX.dll to ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}")
  endif()

  add_executable(example_Messages_Signals Messages_Signals.cpp)

  target_link_libraries(example_Messages_Signals
    ASAM_FIBEX
    ${XercesC_LIBRARY})

endif()

install(
  FILES ${CMAKE_CURRENT_SOURCE_DIR}/Messages_Signals.cpp
  DESTINATION share/doc/ASAM_FIBEX/examples)
