find_library(XercesC_LIBRARY
  NAMES xerces-c-3.1
  PATHS
    "/usr/lib/i386-linux-gnu"
  DOC "Xerces-C++ (http://xerces.apache.org/xerces-c/")

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(XercesC DEFAULT_MSG XercesC_LIBRARY)
get_filename_component(XercesC_LIBRARY_DIRS "${XercesC_LIBRARY}" PATH CACHE)

mark_as_advanced(
  XercesC_LIBRARY
  XercesC_LIBRARY_DIRS)
